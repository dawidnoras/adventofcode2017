import sys
import os
import importlib
from common import challenge_solver


def save_answers(answer_one, answer_two):
    out_path = file_directory + '/out.txt'
    old_answer_one = ''
    old_answer_two = ''
    if os.path.exists(out_path):
        with open(out_path, 'r') as file:
            lines = file.readlines()
            if len(lines) == 2:
                old_answer_one = lines[0].lstrip('result one: ').rstrip('\n')
                old_answer_two = lines[1].lstrip('result two: ').rstrip('\n')

    with open(out_path, 'w') as file:
        file.write('result one: {}\nresult two: {}'.format(answer_one if answer_one is not None else old_answer_one,
                                                           answer_two if answer_two is not None else old_answer_two))


if __name__ == '__main__':
    current_year = 2021
    current_day_number = 1
    current_challenge_number = 1
    year = int(sys.argv[1]) if len(sys.argv) > 1 else current_year
    day_number = int(sys.argv[2]) if len(sys.argv) > 2 else current_day_number
    challenges_to_run = int(sys.argv[3]) if len(
        sys.argv) > 3 else current_challenge_number  # 1 - one, 2 - two, 3 - both

    module_directory = 'challenges.{}.day_{:02d}'.format(year, day_number)
    challenge_module = importlib.import_module('{}.day_{:02d}'.format(module_directory, day_number))
    file_directory = module_directory.replace('.', '/')
    print('\n----Start----\n' + challenge_module.get_date())
    result_one = None
    if challenges_to_run == 1 or challenges_to_run == 3:
        result_one = challenge_solver.run(challenge_module, 1, file_directory)
        print('\n----Result One----\n' + result_one)

    result_two = None
    if challenges_to_run == 2 or challenges_to_run == 3:
        result_two = challenge_solver.run(challenge_module, 2, file_directory)
        print('\n----Result Two----\n' + result_two)

    save_answers(result_one, result_two)



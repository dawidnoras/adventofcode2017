import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    lines = [line.rstrip('\n').lstrip('+') for line in file]
    print(lines)
    return lines


def run_challenge_one(challenge_input):
    frequency = 0
    for number in challenge_input:
        frequency += int(number)
    return str(frequency)


def run_challenge_two(challenge_input):
    frequency = 0
    visited_frequencies = []
    iteration = 0
    while True:
        iteration = iteration + 1
        for number in challenge_input:
            if frequency in visited_frequencies:
                return str(frequency)
            visited_frequencies.append(frequency)
            frequency += int(number)
        print(iteration)

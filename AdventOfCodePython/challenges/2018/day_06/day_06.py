import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.closest_coordinate = None
        self.distance_to_closest_coordinate = None

    def get_distance_to_coordinate(self, coordinate):
        return abs(self.x - coordinate.x) + abs(self.y - coordinate.y)


class Coordinate:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.closest_points = []
        self.has_infinite_area = False

    def get_distance_to_point(self, point):
        return abs(self.x - point.x) + abs(self.y - point.y)


def parse_input(file):
    coordinates = [Coordinate(int(split_value[0]), int(split_value[1])) for split_value in
              [line.rstrip('\n').split(', ') for line in file.readlines()]]
    min_x = coordinates[0].x
    min_y = coordinates[0].y
    max_x = coordinates[0].x
    max_y = coordinates[0].y
    for coordinate in coordinates:
        if coordinate.x < min_x:
            min_x = coordinate.x
        if coordinate.y < min_y:
            min_y = coordinate.y
        if coordinate.x > max_x:
            max_x = coordinate.x
        if coordinate.y > max_y:
            max_y = coordinate.y
    return coordinates, min_x, min_y, max_x, max_y


def run_challenge_one(challenge_input):
    coordinates = challenge_input[0]
    max_x = challenge_input[3]
    max_y = challenge_input[4]

    array = [[Point(i, j) for i in range(max_x + 1)] for j in range(max_y + 1)]
    for column in array:
        for point in column:
            for coordinate in coordinates:
                distance = coordinate.get_distance_to_point(point)
                #  point is closer to coordinate
                if point.distance_to_closest_coordinate is None or distance < point.distance_to_closest_coordinate:
                    if point.closest_coordinate is not None:
                        point.closest_coordinate.closest_points.remove(point)
                    point.closest_coordinate = coordinate
                    point.distance_to_closest_coordinate = distance
                    coordinate.closest_points.append(point)
                else:
                    #  point is closest to a few coordinates
                    if point.distance_to_closest_coordinate is None or distance == point.distance_to_closest_coordinate:
                        if point.closest_coordinate is not None:
                            point.closest_coordinate.closest_points.remove(point)
                        point.closest_coordinate = None
                        point.distance_to_closest_coordinate = distance
            # mark areas as infinite
            if point.x == 0 or point.y == 0 or point.x == max_x or point.y == max_y:
                if point.closest_coordinate is not None:
                    point.closest_coordinate.has_infinite_area = True

    max_length = 0
    for coordinate in coordinates:
        if not coordinate.has_infinite_area:
            coordinate_points_count = len(coordinate.closest_points)
            if coordinate_points_count > max_length:
                max_length = coordinate_points_count

    return str(max_length)


def run_challenge_two(challenge_input):
    coordinates = challenge_input[0]
    max_x = challenge_input[3]
    max_y = challenge_input[4]

    region_size = 0

    array = [[Point(i, j) for i in range(max_x + 1)] for j in range(max_y + 1)]
    for column in array:
        for point in column:
            distance_sum = 0
            for coordinate in coordinates:
                distance = coordinate.get_distance_to_point(point)
                distance_sum = distance_sum + distance
            if distance_sum < 10000:
                region_size = region_size + 1

    return str(region_size)

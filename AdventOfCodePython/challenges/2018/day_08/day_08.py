import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return [int(number) for number in file.read().split(' ')]


class Node:
    def __init__(self):
        self.children = []
        self.meta_data = []

    def get_meta_data_sum(self):
        return sum(self.meta_data)


def create_node(data):
    node = Node()
    children_count = data[0]
    meta_data_count = data[1]
    offset = 2
    #  read children
    for children_index in range(children_count):
        child_node, node_size = create_node(data[offset:])
        node.children.append(child_node)
        offset = offset + node_size

    #  read meta data
    for children_index in range(meta_data_count):
        node.meta_data.append(data[offset])
        offset = offset + 1

    return node, offset


def traverse_children_sum(node):
    meta_data_sum = sum(node.meta_data)
    for child in node.children:
        meta_data_sum = meta_data_sum + traverse_children_sum(child)
    return meta_data_sum


def traverse_children_sum_challenge_two(node):
    if len(node.children) == 0:
        meta_data_sum = sum(node.meta_data)
    else:
        meta_data_sum = 0
        for meta_data in node.meta_data:
            if meta_data == 0:
                continue
            index = meta_data - 1
            if len(node.children) <= index:
                continue
            meta_data_sum = meta_data_sum + traverse_children_sum_challenge_two(node.children[index])

    return meta_data_sum


def run_challenge_one(challenge_input):
    root, offset = create_node(challenge_input)
    meta_data_sum = traverse_children_sum(root)
    return str(meta_data_sum)


def run_challenge_two(challenge_input):
    root, offset = create_node(challenge_input)
    meta_data_sum = traverse_children_sum_challenge_two(root)
    return str(meta_data_sum)

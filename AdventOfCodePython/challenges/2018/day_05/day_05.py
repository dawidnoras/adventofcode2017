import os
from multiprocessing import Process, Queue


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return file.read()


def get_opposite_polarity(unit):
    if ord(unit) < 97:
        return unit.lower()
    return unit.upper()


def run_challenge_one(challenge_input):
    current_index = 0
    while True:
        reduced_polymer = False

        while True:
            character = challenge_input[current_index]
            character2 = get_opposite_polarity(challenge_input[current_index + 1])
            if character == character2:
                reduced_polymer = True
                challenge_input = challenge_input[:current_index] + challenge_input[current_index + 2:]
                current_index = current_index - 1

            current_index = current_index + 1
            if current_index >= len(challenge_input) - 1:
                current_index = 0
                break

        if not reduced_polymer:
            return str(len(challenge_input))


def run_process_for_char(char, challenge_input, queue):
    char2 = get_opposite_polarity(char)
    current_index = 0
    while True:
        if challenge_input[current_index] == char or challenge_input[current_index] == char2:
            challenge_input = challenge_input[:current_index] + challenge_input[current_index + 1:]
            current_index = current_index - 1
        current_index = current_index + 1
        if current_index >= len(challenge_input) - 1:
            break

    length = int(run_challenge_one(challenge_input))
    queue.put(length)


def run_challenge_two(challenge_input):
    processes = []
    queue = Queue()
    for i in range(26):
        char = chr(65 + i)
        challenge_input_copy = challenge_input
        process = Process(target=run_process_for_char, args=(char, challenge_input_copy, queue))
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    min_length = len(challenge_input)
    while not queue.empty():
        computed_length = queue.get()
        if computed_length < min_length:
            min_length = computed_length

    return str(min_length)

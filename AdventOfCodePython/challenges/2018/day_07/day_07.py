import os
import operator


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Step:
    def __init__(self, name):
        self.name = name
        self.dependencies = []
        self.time_left = ord(name) - 4

    def can_execute(self, executed_steps):
        for dependency in self.dependencies:
            found_step = next((step for step in executed_steps if step.name == dependency.name), None)
            if found_step is None:
                return False
        return True

    def __repr__(self):
        return '{}-{}'.format(self.name, self.time_left)


def parse_input(file):
    steps = []
    for line in file.readlines():
        step_name = line[36]
        found_step = next((step for step in steps if step.name == step_name), None)
        if found_step is None:
            found_step = Step(step_name)
            steps.append(found_step)
        step_name = line[5]
        found_dependency_step = next((step for step in steps if step.name == step_name), None)
        if found_dependency_step is None:
            found_dependency_step = Step(step_name)
            steps.append(found_dependency_step)
        found_step.dependencies.append(found_dependency_step)
    return steps


def get_steps_that_can_be_executed(steps, executed_steps):
    steps_that_can_be_executed = []
    for step in steps:
        if step.can_execute(executed_steps):
            steps_that_can_be_executed.append(step)
    return sorted(steps_that_can_be_executed, key=operator.attrgetter('name'))


def run_challenge_one(challenge_input):
    executed_steps = []
    while len(challenge_input) > 0:
        steps_that_can_be_executed = get_steps_that_can_be_executed(challenge_input, executed_steps)
        challenge_input.remove(steps_that_can_be_executed[0])
        executed_steps.append(steps_that_can_be_executed[0])

    result = ''
    for step in executed_steps:
        result = result + step.name
    return result


def run_challenge_two(challenge_input):
    executed_steps = []
    executing_steps = []
    workers_count = 5
    execution_time = 0
    while len(challenge_input) > 0 or len(executing_steps) > 0:
        steps_that_can_be_executed = get_steps_that_can_be_executed(challenge_input, executed_steps)
        while len(steps_that_can_be_executed) > 0 and len(executing_steps) < workers_count:
            challenge_input.remove(steps_that_can_be_executed[0])
            executing_steps.append(steps_that_can_be_executed[0])
            steps_that_can_be_executed.pop(0)
        just_finished_steps = []
        for step in executing_steps:
            step.time_left = step.time_left - 1
            if step.time_left <= 0:
                just_finished_steps.append(step)
        for step in just_finished_steps:
            executing_steps.remove(step)
            executed_steps.append(step)
        execution_time = execution_time + 1
    return str(execution_time)

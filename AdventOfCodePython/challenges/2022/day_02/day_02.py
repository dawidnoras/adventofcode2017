import os
import sys
import itertools
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


ROCK = 'A'
PAPER = 'B'
SCISSORS = 'C'

LOSE = 'A'
DRAW = 'B'
WIN = 'C'

def parse_input(file) -> List[int]:
    diff = ord('A') - ord('X')
    lines = []
    for line in file:
        lines.append([line[0], chr(ord(line[2]) + diff)])
    return lines


def get_score(elf, me):
    elf_score = ord(elf) - ord('A') + 1
    me_score = ord(me) - ord('A') + 1
    if elf == me:
        # draw
        elf_score = elf_score + 3
        me_score = me_score + 3
    elif elf == ROCK and me == SCISSORS or \
         elf == PAPER and me == ROCK or \
         elf == SCISSORS and me == PAPER :
        # elf won
        elf_score = elf_score + 6
    else:
        me_score = me_score + 6
    return elf_score, me_score


def test_score(elf, me, elf_score, me_score):
    e, m = get_score(elf, me)
    if e != elf_score or m != me_score:
        print('score does not match for {} {}. got {} {} expected {} {}'.format(elf, me, e, m, elf_score, me_score))


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    score = 0
    for round in challenge_input:
        elf_score, me_score = get_score(round[0], round[1])
        score = score + me_score
    return str(score)


def get_sign(elf, outcome):
    if outcome == DRAW:
        return elf

    if elf == ROCK:
        if outcome == LOSE:
            return SCISSORS
        return PAPER
    if elf == PAPER:
        if outcome == LOSE:
            return ROCK
        return SCISSORS
    if outcome == LOSE:
        return PAPER
    return ROCK


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    score = 0
    for round in challenge_input:
        elf_score, me_score = get_score(round[0], get_sign(round[0], round[1]))
        score = score + me_score
    return str(score)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import itertools
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    lines = []
    for line in file:
        line = line.rstrip('\n')
        half_size = int(len(line) / 2)
        lines.append([line[:half_size], line[half_size:]])
    return lines


def get_priority(item):
    if item >= 'a':
        return ord(item) - ord('a') + 1
    return ord(item) - ord('A') + 27


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    score = 0
    for sack in challenge_input:
        common_items = [item for item in sack[0] if item in sack[1]]
        score = score + get_priority(common_items[0])
    return str(score)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    score = 0
    for i in range(0, len(challenge_input), 3):
        sack_0 = challenge_input[i][0] + challenge_input[i][1]
        sack_1 = challenge_input[i + 1][0] + challenge_input[i + 1][1]
        sack_2 = challenge_input[i + 2][0] + challenge_input[i + 2][1]
        common_items = [item for item in sack_0 if item in sack_1]
        common_items = [item for item in common_items if item in sack_2]
        score = score + get_priority(common_items[0])

    return str(score)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
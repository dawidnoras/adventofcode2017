import os
import sys
import itertools
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    elves = []
    elf = []
    for line in file:
        if line == '\n':
            elves.append(elf)
            elf = []
            continue
        elf.append(int(line))
    return elves


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    sum_max = 0
    for elf in challenge_input:
        new_sum = sum(elf)
        if new_sum > sum_max:
            sum_max = new_sum
    return str(sum_max)


def get_max(elves):
    sum_max = 0
    index_max = 0
    for index, elf in enumerate(elves):
        new_sum = sum(elf)
        if new_sum > sum_max:
            sum_max = new_sum
            index_max = index
    return sum_max, index_max

@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    sum_max = 0
    for _ in range(3):
        value, index = get_max(challenge_input)
        sum_max = sum_max + value
        del challenge_input[index]

    return str(sum_max)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
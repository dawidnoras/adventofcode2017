import sys
import os
import copy
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


FLOOR = '.'
OCCUPIED = '#'
EMPTY = 'L'

def parse_input(file):
    result = []
    lines = [line.rstrip('\n') for line in file]
    for line in lines:
        chars = []
        for char in line:
            chars.append(char)
        result.append(chars)
    return result


def number_of_adjenced_occupied_seats_ch_one(tab, line, char):
    height = len(tab)
    width = len(tab[0])
    
    count = 0
    for y in range(-1, 2):
        if 0 <= line + y < height:
            for x in range(-1, 2):
                if 0 <= char + x < width and \
                        not (x == 0 and y == 0) and \
                        tab[line + y][char + x] == OCCUPIED:
                    count = count + 1
    return count


def print_it(tab):
    print('-----------------')
    for line in tab:
        print(line)
            

@aoc.timer
def run_challenge_one(challenge_input):
    prev_tab = challenge_input
    
    while True:
        new_tab = copy.deepcopy(prev_tab)
        # print_it(new_tab)
        state_changed = False
        
        for y, line in enumerate(prev_tab):
            for x, char in enumerate(line):
                if char == EMPTY:
                    if number_of_adjenced_occupied_seats_ch_one(prev_tab, y, x) == 0:
                        new_tab[y][x] = OCCUPIED
                        state_changed = True
                elif char == OCCUPIED:
                    if number_of_adjenced_occupied_seats_ch_one(prev_tab, y, x) >= 4:
                        new_tab[y][x] = EMPTY
                        state_changed = True

        if state_changed:
            prev_tab = new_tab
        else:
            break
        
    # number of occupied seats
    s = 0
    for line in prev_tab:
        for char in line:
            if char == OCCUPIED:
                s = s + 1
                
    return str(s)

def number_of_seen_occupied_seats_ch_two(tab, line, char, height = None, width = None):
    height = len(tab) if height is None else height
    width = len(tab[0]) if width is None else width
    
    directions = [
        [0, 1],
        [1, 1],
        [1, 0],
        [1, -1],
        [0, -1],
        [-1, -1],
        [-1, 0],
        [-1, 1],
    ]
    
    count = 0
    for direction in directions:
        x = char
        y = line
        while True:
            if y < 0 or y >= height or x < 0 or x >= width:
                break
            if not (x == char and y == line):
                if tab[y][x] == OCCUPIED:
                    count = count + 1
                    break
                if tab[y][x] == EMPTY:
                    break
            y = y + direction[0]
            x = x + direction[1]
    
    return count


@aoc.timer
def run_challenge_two(challenge_input):
    prev_tab = challenge_input
    
    while True:
        new_tab = copy.deepcopy(prev_tab)
        # print_it(new_tab)
        state_changed = False
        
        for y, line in enumerate(prev_tab):
            for x, char in enumerate(line):
                if char == EMPTY:
                    if number_of_seen_occupied_seats_ch_two(prev_tab, y, x) == 0:
                        new_tab[y][x] = OCCUPIED
                        state_changed = True
                elif char == OCCUPIED:
                    if number_of_seen_occupied_seats_ch_two(prev_tab, y, x) >= 5:
                        new_tab[y][x] = EMPTY
                        state_changed = True

        if state_changed:
            prev_tab = new_tab
        else:
            break
        
    # number of occupied seats
    s = 0
    for line in prev_tab:
        for char in line:
            if char == OCCUPIED:
                s = s + 1
                
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
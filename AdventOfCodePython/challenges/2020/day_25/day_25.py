import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    return [int(line.rstrip('\n')) for line in file]


def transform_subject_number(subject_number, loop_size):
    value = 1
    for i in range(loop_size):
        value = value * subject_number
        value = value % 20201227
    return value


@aoc.timer
def run_challenge_one(challenge_input):
    card_public_key = challenge_input[0]
    door_public_key = challenge_input[1]
    
    subject_number = 7
    
    loop_size = 1
    value = 1
    while True:
        value = value * subject_number
        value = value % 20201227
        if value == card_public_key:
            card_loop_size = loop_size
            break
        loop_size = loop_size + 1
    
    loop_size = 1
    value = 1
    while True:
        value = value * subject_number
        value = value % 20201227
        if value == door_public_key:
            door_loop_size = loop_size
            break
        loop_size = loop_size + 1
    
    result = transform_subject_number(door_public_key, card_loop_size)
    result2 = transform_subject_number(card_public_key, door_loop_size)

    return str(result)


@aoc.timer
def run_challenge_two(challenge_input):

    return str(0)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_ONE)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
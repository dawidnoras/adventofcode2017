import sys
import os
from typing import List
import itertools
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List:
    input_list = []
    for line in file:
        input_list.append(int(line.rstrip('\n')))
    return input_list


@aoc.timer
def run_challenge_one(challenge_input: List):
    preamble = 25
    
    not_matching_index = 0
    for i in range(preamble, len(challenge_input)):
        start = i - preamble
        end = i + preamble
        part_to_check = challenge_input[start:end]
        combinations = itertools.combinations(part_to_check, 2)
        found = False
        for a, b in combinations:
            if a + b == challenge_input[i]:
                found = True
                break
        if not found:
            not_matching_index = i
            break
    
    return str(challenge_input[not_matching_index])


@aoc.timer
def run_challenge_two(challenge_input: List):
    preamble = 25
    
    not_matching_index = 0
    for i in range(preamble, len(challenge_input)):
        start = i - preamble
        end = i + preamble
        part_to_check = challenge_input[start:end]
        combinations = itertools.combinations(part_to_check, 2)
        found = False
        for a, b in combinations:
            if a + b == challenge_input[i]:
                found = True
                break
        if not found:
            not_matching_index = i
            break

    not_matching_number = challenge_input[not_matching_index]
    
    # part 2
    index_start = -1
    index_end = -1
    
    for i in range(len(challenge_input)):
        index_start = i
        
        s = 0
        j = i
        while s < not_matching_number:
            s = s + challenge_input[j]
            j = j + 1
            
        if s == not_matching_number:
            index_end = j - 1
            break
    
    smaller_number = min(challenge_input[index_start:index_end + 1])
    largest_number = max(challenge_input[index_start:index_end + 1])
    result = smaller_number + largest_number
    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
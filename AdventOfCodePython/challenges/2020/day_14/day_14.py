import sys
import os
from typing import List
import re
from itertools import chain, combinations
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Mask:
    def __init__(self, bit_str):
        self.set_bit_str(bit_str)

    def mask(self, value):
        value = value | self.set_mask
        value = value & self.reset_mask
        return value

    def mask_2(self, value):
        value = value | self.set_mask
        # value = value & self.reset_mask
        return value

    def set_bit_str(self, value):
        self.bit_str = value
        set_str = self.bit_str.replace('X', '0')
        self.set_mask = int(set_str, 2)
        reset_str = self.bit_str.replace('X', '1')
        self.reset_mask = int(reset_str, 2)
    
    def set_bit_str_2(self, value):
        self.set_bit_str(value)
        self.floating_bits = [m.start() for m in re.finditer('X', value)]
    
    def __repr__(self):
        return self.bit_str


MASK = Mask('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')


class MaskAssignment:
    def __init__(self, bit_str):
        self.bit_str = bit_str
    
    def execute(self, memory, mask):
        MASK.set_bit_str(self.bit_str)
        
    def execute_2(self, memory, mask):
        MASK.set_bit_str_2(self.bit_str)
        
    def __repr__(self):
        return "mask = {}".format(self.bit_str)
    
    
def all_subsets(ss):
    return chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))


class MemoryAssignment:
    def __init__(self, index, value):
        self.index = index
        self.value = value
        
    def execute(self, memory, mask):
        masked_value = mask.mask(self.value)
        memory[self.index] = masked_value
        
    def execute_2(self, memory, mask):
        masked_index = mask.mask_2(self.index)
        floating_bits_combinations = all_subsets(mask.floating_bits)
        # rexet 'X' to 0
        addresses = []
        for i in mask.floating_bits:
            masked_index = masked_index & ~(1 << 35-i)
        for combination in floating_bits_combinations:
            new_index = masked_index
            for x in combination:
                new_index = new_index | (1 << 35 - x)
            addresses.append(new_index)
        
        for address in addresses:
            memory[address] = self.value
        
    def __repr__(self):
        return "mem[{}] = {}".format(self.index, self.value)


def parse_input(file):
    mask_pattern = re.compile('mask = ([\\d\\w]+)')
    memory_pattern = re.compile('mem\\[(\\d+)\\] = (\\d+)')
    input_list = []
    for line in file:
        match = mask_pattern.match(line)
        if match is not None:
            mask_str = match.groups()[0]
            mask = MaskAssignment(mask_str)
            input_list.append(mask)
        else:
            match = memory_pattern.match(line)
            (index, value) = match.groups()
            mem = MemoryAssignment(int(index), int(value))
            input_list.append(mem)
        
    return input_list


@aoc.timer
def run_challenge_one(challenge_input):
    code = challenge_input
    max_mem = max(code, key=lambda x: x.index if hasattr(x, 'index') else -1)
    memory = [0] * (max_mem.index + 1)
    
    for line in code:
        line.execute(memory, MASK)
    
    s = sum(memory)
    return str(s)


@aoc.timer
def run_challenge_two(challenge_input):
    code = challenge_input
    # max_mem = max(code, key=lambda x: x.index if hasattr(x, 'index') else -1)
    memory = {}
    
    for line in code:
        line.execute_2(memory, MASK)
    
    s = 0
    for key in memory:
        s = s + memory[key]
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
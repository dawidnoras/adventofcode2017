import os
import sys
import itertools
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    return [int(line) for line in file]


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    for a, b in itertools.combinations(challenge_input, 2):
        if a + b == 2020:
            return str(a * b)
    return str(0)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    for a, b, c in itertools.combinations(challenge_input, 3):
        if a + b + c == 2020:
            return str(a * b * c)
    return str(0)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
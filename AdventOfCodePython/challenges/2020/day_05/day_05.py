import sys
import os
from typing import List
import math
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Seat:
    def __init__(self, string):
        self.string = string        

    def get_row_collumn(self):
        #row
        row_number_range = [0, 127]
        for r in range(7):
            s = self.string[r]
            half_range = math.ceil((row_number_range[1] - row_number_range[0]) / 2)
            if s == 'F':
                row_number_range[1] = row_number_range[1] - half_range
            elif s == 'B':
                row_number_range[0] = row_number_range[0] + half_range
        
        if row_number_range[0] != row_number_range[1]:
            print('wrong row calculation {}'.format(self.string[r]))
            
        #collumn
        collumn_number_range = [0, 7]
        for c in range(7, 10):
            s = self.string[c]
            half_range = math.ceil((collumn_number_range[1] - collumn_number_range[0]) / 2)
            if s == 'L':
                collumn_number_range[1] = collumn_number_range[1] - half_range
            elif s == 'R':
                collumn_number_range[0] = collumn_number_range[0] + half_range
        
        if collumn_number_range[0] != collumn_number_range[1]:
            print('wrong collumn calculation {}'.format(self.string[c]))
        
        self.row = row_number_range[0]
        self.collumn = collumn_number_range[0]
        return self.row, self.collumn
    
    def get_seat_id(self):
        return self.row * 8 + self.collumn


def parse_input(file) -> List[Seat]:
    return [line.rstrip('\n') for line in file]


@aoc.timer
def run_challenge_one(challenge_input: List[Seat]):
    max_id = 0
    for seat_str in challenge_input:
        seat = Seat(seat_str)
        row, collumn = seat.get_row_collumn()
        seat_id = seat.get_seat_id()
        
        print('"{}"->[{},{}]\t=\t{}'.format(seat_str, row, collumn, seat_id))
        if seat_id > max_id:
            max_id = seat_id
    
    return str(max_id)


@aoc.timer
def run_challenge_two(challenge_input: List[Seat]):
    
    available_seats = []
    available_seat_ids = []
    for seat_str in challenge_input:
        seat = Seat(seat_str)
        seat_id = seat.get_seat_id()
        available_seats.append(seat)
        available_seat_ids.append(seat_id)
        
    missing_id = 0
    available_seat_ids.sort()
    previous_id = available_seat_ids[0] - 1
    for _id in available_seat_ids:
        if previous_id + 2 == _id:
            missing_id = _id - 1
            break
        previous_id = _id
        
    return str(missing_id)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Field:
    def __init__(self, name, range_one_min, range_one_max, range_two_min, range_two_max):
        self.name = name
        self.range_one_min = range_one_min
        self.range_one_max = range_one_max
        self.range_two_min = range_two_min
        self.range_two_max = range_two_max

    def is_in_range(self, value):
        return self.range_one_min <= value <= self.range_one_max or \
               self.range_two_min <= value <= self.range_two_max

    def __repr__(self):
        return '{' + '{}: {}-{} or {}-{}'.format(self.name, self.range_one_min, self.range_one_max, self.range_two_min, self.range_two_max) + '}'


class Ticket:
    def __init__(self, fields):
        self.fields = fields
        
    def __repr__(self):
        return '{' + str(self.fields) + '}'
        

def parse_input(file):
    field_name_pattern = re.compile('([\\w ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)')
    ticket_numbers_pattern = re.compile('(\\d+)')
    fields = []
    nerby_tickets = []
    state = 'fields'
    for line in file:
        if state == 'fields':
            match = field_name_pattern.match(line)
            if match is not None:
                (field_name, range_one_min, range_one_max, range_two_min, range_two_max) = match.groups()
                fields.append(Field(field_name, int(range_one_min), int(range_one_max), int(range_two_min), int(range_two_max)))
            else:
                state = 'my_ticket_label'
        elif state == 'my_ticket_label':
            state = 'my_ticket_values'
        elif state == 'my_ticket_values':
            match = ticket_numbers_pattern.match(line)
            if match is not None:
                ticket_numbers = []
                for match in re.finditer(ticket_numbers_pattern, line):
                    num = match.groups()[0]
                    ticket_numbers.append(int(num))
                my_ticket = Ticket(ticket_numbers)
            else:
                state = 'nerby_tickets_label'
        elif state == 'nerby_tickets_label':
            state = 'nerby_tickets_values'
        elif state == 'nerby_tickets_values':
            match = ticket_numbers_pattern.match(line)
            if match is not None:
                ticket_numbers = []
                for match in re.finditer(ticket_numbers_pattern, line):
                    num = match.groups()[0]
                    ticket_numbers.append(int(num))
                nerby_tickets.append(Ticket(ticket_numbers))
    return fields, my_ticket, nerby_tickets


@aoc.timer
def run_challenge_one(challenge_input):
    fields, my_ticket, nerby_tickets = challenge_input
    
    s = 0
    for ticket in nerby_tickets:
        for field in ticket.fields:
            could_be_valid = False
            for check_field in fields:
                if check_field.is_in_range(field):
                    could_be_valid = True
                    break
            if not could_be_valid:
                s = s + field
    
    return str(s)


@aoc.timer
def run_challenge_two(challenge_input):
    check_fields, my_ticket, nerby_tickets = challenge_input
    
    # remove invalid tickets
    invalid_tickets= []
    for ticket in nerby_tickets:
        for field in ticket.fields:
            could_be_valid = False
            for check_field in check_fields:
                if check_field.is_in_range(field):
                    could_be_valid = True
                    break
            if not could_be_valid:
                invalid_tickets.append(ticket)
                break
    for ticket in invalid_tickets:
        nerby_tickets.remove(ticket)      
              
    # available fields
    available_fields = []
    for _ in range(len(check_fields)):
        fields = []
        for field in check_fields:
            fields.append(field)
        available_fields.append(fields)
            
     
    for i, check_field in enumerate(available_fields):
        fields_to_remove = []
        for field in check_field:
            for ticket in nerby_tickets:
                is_valid = field.is_in_range(ticket.fields[i])
                if not is_valid:
                   fields_to_remove.append(field)
                   break
        for field in fields_to_remove:
            check_field.remove(field)
        
    
    reduced_fields = []     
    while True:
        reduction_fields = []
        for entry in available_fields:
            if len(entry) == 1 and entry[0] not in reduced_fields:
                reduction_fields.append(entry[0])
                break
        if len(reduction_fields) == 0:
            break
        for reduction_field in reduction_fields:
            for entry in available_fields:
                if len(entry) > 1 and reduction_field in entry:
                    entry.remove(reduction_field)
            reduced_fields.append(reduction_field)
              
    s = 1
    for i, entry in enumerate(available_fields):
        if entry[0].name.startswith('departure'):
            s = s * my_ticket.fields[i]
      
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
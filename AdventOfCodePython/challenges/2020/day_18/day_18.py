import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    return [line.rstrip('\n') for line in file.readlines()]


@aoc.timer
def run_challenge_one(challenge_input):
    number_only_pattern = re.compile('^(\\d+)$')
    inside_most_prentecies = re.compile('\\([\\d\\+\\*\\- ]*\\)|^[\\d\\+\\*\\- ]*$')
    inside_prentecies_numbers_and_signs = re.compile('\\d+|\\+|\\*|\\-')
    
    s = 0
    for expression in challenge_input:
        str_exp = expression
        while True:
            if number_only_pattern.match(str_exp) is not None:
                break
            matches = re.findall(inside_most_prentecies, str_exp)
            for exp in matches:
                matches_inside = re.findall(inside_prentecies_numbers_and_signs, exp)
                num = int(matches_inside[0])
                for i in range(1, len(matches_inside), 2):
                    if matches_inside[i] == '+':
                        num = num + int(matches_inside[i + 1])
                    elif matches_inside[i] == '-':
                        num = num - int(matches_inside[i + 1])
                    elif matches_inside[i] == '*':
                        num = num * int(matches_inside[i + 1])
                str_exp = str_exp.replace(exp, str(num))
        s = s + num
            
    return str(s)


@aoc.timer
def run_challenge_two(challenge_input):
    number_only_pattern = re.compile('^(\\d+)$')
    inside_most_prentecies = re.compile('\\([\\d\\+\\*\\- ]*\\)|^[\\d\\+\\*\\- ]*$')
    inside_prentecies_numbers_and_signs = re.compile('\\d+|\\+|\\*|\\-')
    inside_prentecies_add_expressions = re.compile('(\\d+) \\+ (\\d+)')
    inside_prentecies_multiply_expressions = re.compile('(\\d+) \\* (\\d+)')
    
    
    s = 0
    for expression in challenge_input:
        str_exp = expression
        while True:
            if number_only_pattern.match(str_exp) is not None:
                break
            matches = re.findall(inside_most_prentecies, str_exp)
            for exp in matches:
                str_inside_exp = exp
                while True:
                    matches_add_inside = re.findall(inside_prentecies_add_expressions, str_inside_exp)
                    if len(matches_add_inside) == 0:
                        break
                    for m in matches_add_inside:
                        num = int(m[0]) + int(m[1])
                        str_to_replace = '{} + {}'.format(m[0], m[1])
                        str_inside_exp = str_inside_exp.replace(str_to_replace, str(num), 1)
                while True:
                    matches_multiply_inside = re.findall(inside_prentecies_multiply_expressions, str_inside_exp)
                    if len(matches_multiply_inside) == 0:
                        break
                    for m in matches_multiply_inside:
                        num = int(m[0]) * int(m[1])
                        str_inside_exp = str_inside_exp.replace('{} * {}'.format(m[0], m[1]), str(num), 1)
                str_exp = str_exp.replace(exp, str(num))
        s = s + int(str_exp)
    
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
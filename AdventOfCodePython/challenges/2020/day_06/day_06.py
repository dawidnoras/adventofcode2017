import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Group:
    def __init__(self, quesions: List):
        self.all_questions = quesions
        # self.positive_questions = []
        # for q in quesions:
        #     if q not in self.positive_questions:
        #         self.positive_questions.append(q)
        
        self.all_positive_questions = quesions[0]
        for q in quesions:
            if q == '':
                self.all_positive_questions = quesions[1]
                continue
            list1_as_set = set(q)
            intersection = list1_as_set.intersection(self.all_positive_questions)
            self.all_positive_questions = list(intersection)
        


def parse_input(file) -> List[Group]:
    input_list = []
    questions = []
    for line in file:
        if line == '\n':
            input_list.append(Group(questions))
            questions = []
        questions.append(line.rstrip('\n'))            
        
    return input_list


@aoc.timer
def run_challenge_one(challenge_input: List[Group]):
    count = 0
    
    for group in challenge_input:
        print(group.positive_questions)
        count = count + len(group.positive_questions) 

    return str(count)


@aoc.timer
def run_challenge_two(challenge_input: List[Group]):
    count = 0
    
    for group in challenge_input:
        print(group.all_positive_questions)
        count = count + len(group.all_positive_questions) 
        
    return str(count)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Program():
    def __init__(self, code):
        self.code = code
        self.ip = 0
        self.acc = 0
        self.code_len = len(code)

    def run_until_inf_loop(self):
        instructions_visited = []
        while True:
            if self.ip in instructions_visited:
                return
            if self.ip >= self.code_len:
                return
            
            instructions_visited.append(self.ip)
            
            instruction = self.code[self.ip]
            if instruction.operation == 'nop':
                self.ip = self.ip + 1
                continue 
            if instruction.operation == 'acc':
                self.acc = self.acc + instruction.argument
                self.ip = self.ip + 1
                continue
            if instruction.operation == 'jmp':
                self.ip = self.ip + instruction.argument
                continue        


class Instruction:
    def __init__(self, operation, argument):
        self.operation = operation
        self.argument = argument


def parse_input(file) -> Program:
    code = []
    for line in file:
        line = line.rstrip('\n')
        split = line.split()
        instruction = Instruction(split[0], int(split[1]))
        code.append(instruction)
    program = Program(code)
    return program


@aoc.timer
def run_challenge_one(challenge_input: Program):
    program = challenge_input
    
    program.run_until_inf_loop()
    
    return str(program.acc)


@aoc.timer
def run_challenge_two(challenge_input: Program):
    original_program = challenge_input
    
    result_program = None
    for i in range(len(original_program.code)):
        code = []
        for inst in original_program.code:
            code.append(Instruction(inst.operation, inst.argument))
            
        instruction = code[i]
        if instruction.operation == 'acc':
            continue
        elif instruction.operation == 'jmp':
            instruction.operation = 'nop'
        elif instruction.operation == 'nop':
            instruction.operation = 'jmp'
        
        program = Program(code)
        program.run_until_inf_loop()
        if program.ip == program.code_len:
            result_program = program
            break
    
    return str(result_program.acc)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Bag:
    def __init__(self, adjective: str, color: str, capacity_list: list):
        self.adjective = adjective
        self.color = color
        self.capacity_list = capacity_list

    def __str__(self):
        return '{} {}'.format(self.adjective, self.color)

    def get_parent_bag_list(self, bags):
        parent_bags = []
        for bag in bags:
            for cap in bag.capacity_list:
                cap_bag = cap['bag']
                if cap_bag.color == self.color and cap_bag.adjective == self.adjective:
                    parent_bags.append(bag)
                    more_parrent_bags = bag.get_parent_bag_list(bags)
                    parent_bags.extend(more_parrent_bags)
        return parent_bags

    def get_number_of_bags(self):        
        bag_count = 1
        for cap in self.capacity_list:
            bag = cap['bag']
            count = cap['count']
            bag_count = bag_count + count * bag.get_number_of_bags()
        return bag_count

# (\\d+) (\\w+) (\\w+) bags*
def parse_input(file) -> List[Bag]:
    patern_contains = re.compile('^(\\w+) (\\w+) bags contain ([\\w ,]+).$')
    patern_not_contains = re.compile('^(\\w+) (\\w+) bags contain no other bags.$')
    pattern_capacity = re.compile('(\\d+) (\\w+) (\\w+) bags*')
    
    lines = file.readlines()
    
    # create all bag list
    bag_list = []
    for line in lines:
        line = line.rstrip('\n')
        match = patern_contains.match(line)
        if match is not None:
            (adjective, color, _) = match.groups()
            bag = Bag(adjective, color, [])
            bag_list.append(bag)
    
    # populate their capacity_list
    index = 0
    for line in lines:
        line = line.rstrip('\n')
        match = patern_not_contains.match(line)
        if match is None:
            match = patern_contains.match(line)
            (adjective, color, capacity_str) = match.groups()
            bag = bag_list[index]
            
            capacity_list = []
            for match_object in re.finditer(pattern_capacity, capacity_str):
                (count, adjective, color) = match_object.groups()
                inside_bag = next(filter(lambda x: x.adjective == adjective and x.color == color, bag_list))
                capacity_entry = {
                    'bag': inside_bag,
                    'count': int(count)
                }
                capacity_list.append(capacity_entry)
            bag.capacity_list = capacity_list
        index = index + 1
        
    return bag_list


@aoc.timer
def run_challenge_one(challenge_input: List[Bag]):
    adjective = 'shiny'
    color = 'gold'
    
    match_bag_lambda = lambda x, c, a: x.adjective == a and x.color == c

    bag = next(filter(lambda x: match_bag_lambda(x, color, adjective), challenge_input))
    can_contain_my_bag = bag.get_parent_bag_list(challenge_input)
    can_contain_my_bag = list(set(can_contain_my_bag))
    
    return str(len(can_contain_my_bag))


@aoc.timer
def run_challenge_two(challenge_input: List[Bag]):
    adjective = 'shiny'
    color = 'gold'
    match_bag_lambda = lambda x, c, a: x.adjective == a and x.color == c
    my_bag = next(filter(lambda x: match_bag_lambda(x, color, adjective), challenge_input))
    
    bag_count = my_bag.get_number_of_bags() - 1
    
    return str(bag_count)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
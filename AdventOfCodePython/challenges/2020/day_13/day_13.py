import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class XClass:
    def __init__(self):
        pass


def parse_input(file):
    # for part 1
    # p = re.compile('(\\d+)')
    # timestamp = int(file.readline().rstrip('\n'))
    # buss_ids = [int(id) for id in re.findall(p, file.readline())]
    # return timestamp, buss_ids
    
    # for part 2
    timestamp = int(file.readline().rstrip('\n'))
    busses = [int(id if id != 'x' else 0) for id in file.readline().rstrip('\n').split(',')]
    return timestamp, busses


@aoc.timer
def run_challenge_one(challenge_input):
    timestamp, buss_ids = challenge_input
    
    t = timestamp
    found_buss_id = None
    while found_buss_id is None:
        for buss_id in buss_ids:
            if t % buss_id == 0:
                found_buss_id = buss_id
                break
        t = t + 1
    
    t = t - 1
    wait_time = t - timestamp
    result = wait_time * found_buss_id

    return str(result)


def lcm(x, y):
    from fractions import gcd # or can import gcd from `math` in Python 3
    return x * y // gcd(x, y)


@aoc.timer
def run_challenge_two(challenge_input):
    timestamp, busses = challenge_input
    
    t = 100000000000000
    s = 1
    i = 0
    while i < len(busses):
        if busses[i] == 0:
            i = i + 1
            continue
        if (t + i) % busses[i] == 0:
            s = s * busses[i]
            i = i + 1
        else:
            t = t + s
    
    return str(t)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
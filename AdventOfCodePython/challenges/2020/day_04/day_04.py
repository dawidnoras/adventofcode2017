import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Passport:
    def __init__(self, fields_str: List[str]):
        self.fields = []
        for field_str in fields_str:
            parts = field_str.split(':')
            self.fields.append({'field': parts[0], 'value': parts[1]})

    def is_valid_part_one(self, required_fields):
        for required_field in required_fields:
            field = next((x for x in self.fields if x['field'] == required_field), None)
            if field is None:
                return False
        return True
    
    def is_valid_part_two(self, required_fields):
        for required_field in required_fields:
            field = next((x for x in self.fields if x['field'] == required_field['field']), None)
            if field is None or not required_field['validation'](field):
                return False
        return True


def parse_input(file) -> List[Passport]:
    input_list = []
    new_passport_fields = []
    for line in file:
        if line == '\n':
            input_list.append(Passport(new_passport_fields))
            new_passport_fields = []
        fields = line.split()
        for field in fields:
            new_passport_fields.append(field)
        
    return input_list


@aoc.timer
def run_challenge_one(challenge_input: List[Passport]):
    required_fields = [
        'ecl',
        'pid',
        'eyr',
        'hcl',
        'byr',
        'iyr',
        'hgt',
    ]
    
    counter = 0
    for passport in challenge_input:
        if passport.is_valid_part_one(required_fields):
            counter = counter + 1
    return str(counter)


@aoc.timer
def run_challenge_two(challenge_input: List[Passport]):
    def is_ecl_valid(x):
        valid_inputs = [
            'amb', 
            'blu', 
            'brn', 
            'gry', 
            'grn', 
            'hzl',
            'oth',
        ]
        return x['value'] in valid_inputs
    
    def is_pid_valid(x):
        p = re.compile('^(\\d+)$')
        match = p.match(x['value'])
        if match is None:
            return False
        value = match.group()
        return len(value) == 9
    
    def is_eyr_valid(x):
        p = re.compile('^(\\d+)$')
        match = p.match(x['value'])
        if match is None:
            return False
        value = match.group()
        int_value = int(value)
        return 2020 <= int_value <= 2030 and len(value) == 4
    
    def is_hgt_valid(x):
        p = re.compile('^(\\d+)cm$')
        match = p.match(x['value'])
        if match is None:
            p = re.compile('^(\\d+)in$')
            match = p.match(x['value'])
            if match is None:
                return False
            value = match.group()
            int_value = int(value[:-2])
            return 59 <= int_value <= 76
        value = match.group()
        int_value = int(value[:-2])
        return 150 <= int_value <= 193
    
    def is_hcl_valid(x):
        p = re.compile('^#(\\d|[a-f])+$')
        match = p.match(x['value'])
        return match is not None
    
    def is_byr_valid(x):
        p = re.compile('^(\\d+)$')
        match = p.match(x['value'])
        if match is None:
            return False
        value = match.group()
        int_value = int(value)
        return 1920 <= int_value <= 2002 and len(value) == 4
    
    def is_iyr_valid(x):
        p = re.compile('^(\\d+)$')
        match = p.match(x['value'])
        if match is None:
            return False
        value = match.group()
        int_value = int(value)
        return 2010 <= int_value <= 2020 and len(value) == 4
    
    required_fields = [
        {'field': 'ecl', 'validation': is_ecl_valid},
        {'field': 'pid', 'validation': is_pid_valid},
        {'field': 'eyr', 'validation': is_eyr_valid},
        {'field': 'hcl', 'validation': is_hcl_valid},
        {'field': 'byr', 'validation': is_byr_valid},
        {'field': 'iyr', 'validation': is_iyr_valid},
        {'field': 'hgt', 'validation': is_hgt_valid},
    ]
    
    counter = 0
    for passport in challenge_input:
        if passport.is_valid_part_two(required_fields):
            counter = counter + 1
    return str(counter)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

P1 = 'P1'
P2 = 'P2'


def parse_input(file):
    deck_1 = []
    deck_2 = []
    deck = []
    
    state = '1'
    for line in file:
        if state == '1':
            state = 'd_1'
            deck = deck_1
        elif state in ['d_1', 'd_2']:
            if line == '\n':
                state = '2'
            else:
                deck.append(int(line.rstrip('\n')))
        elif state == '2':
            state = 'd_2'
            deck = deck_2
    return deck_1, deck_2      


@aoc.timer
def run_challenge_one(challenge_input):
    deck_1, deck_2 = challenge_input
    
    while len(deck_1) > 0 and len(deck_2) > 0:
        card_1 = deck_1.pop(0)
        card_2 = deck_2.pop(0)
        if card_1 > card_2:
            deck_1.append(card_1)
            deck_1.append(card_2)
        else:
            deck_2.append(card_2)
            deck_2.append(card_1)

    result = 0
    winner_deck = deck_1 if len(deck_2) == 0 else deck_2
    for i, card in enumerate(reversed(winner_deck)):
        result = result + (i + 1) * card
    return str(result)


def play_game(deck_1, deck_2):
    memory = set()
    
    while len(deck_1) > 0 and len(deck_2) > 0:
        # recursive combat rule 1
        round_state = (tuple(deck_1), tuple(deck_2))
        if round_state in memory:
            return P1, deck_1
        memory.add(round_state)
        
        card_1 = deck_1.pop(0)
        card_2 = deck_2.pop(0)
        # recursive combat rule 2
        if len(deck_1) >= card_1 and len(deck_2) >= card_2:
            new_deck_1 = deck_1[:card_1]
            new_deck_2 = deck_2[:card_2]
            winner, winner_deck = play_game(new_deck_1, new_deck_2)
            if winner == P1:
                deck_1.append(card_1)
                deck_1.append(card_2)
            else:
                deck_2.append(card_2)
                deck_2.append(card_1)
            continue
        
        if card_1 > card_2:
            deck_1.append(card_1)
            deck_1.append(card_2)
        else:
            deck_2.append(card_2)
            deck_2.append(card_1)

    winner = P1 if len(deck_2) == 0 else P2
    winner_deck = deck_1 if len(deck_2) == 0 else deck_2
    return winner, winner_deck


@aoc.timer
def run_challenge_two(challenge_input):
    deck_1, deck_2 = challenge_input

    winner, winner_deck = play_game(deck_1, deck_2)
    result = 0
    for i, card in enumerate(reversed(winner_deck)):
        result = result + (i + 1) * card    
    
    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
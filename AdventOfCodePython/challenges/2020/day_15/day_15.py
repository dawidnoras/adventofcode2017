import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    return [int(line.rstrip('\n')) for line in file.readline().split(',')]


@aoc.timer
def run_challenge_one(challenge_input):
    numbers = challenge_input

    spoken_numbers = [0] * 2020
    for turn in range(len(numbers)):
        spoken_numbers[turn] = numbers[turn]
        
    for turn in range(len(numbers), 2020):
        last_number = spoken_numbers[turn - 1]
        last_number_last_round = None
        for i in reversed(range(0, turn - 1)):
            if spoken_numbers[i] == last_number:
                last_number_last_round = i
                break
        if last_number_last_round is None:
            spoken_numbers[turn] = 0
        else:
            spoken_numbers[turn] = turn - 1 - last_number_last_round
        
    result = spoken_numbers[len(spoken_numbers) - 1]
    return str(result)


@aoc.timer
def run_challenge_two(challenge_input):
    numbers = challenge_input
    max_turns = 30000000
        
    last_spoken_number_turn_map = {}
    for turn in range(len(numbers) - 1):
        last_spoken_number_turn_map[numbers[turn]] = turn
    
    number = numbers[len(numbers) - 1]
    for turn in range(len(numbers), max_turns):
        last_number_turn = last_spoken_number_turn_map.get(number, None)
        # add latest number
        last_spoken_number_turn_map[number] = turn - 1
        if last_number_turn is None:
            number = 0
        else:
            number = turn - 1 - last_number_turn            
            
        if turn % 200000 == 0:
            print('turn: {}'.format(turn))
          
    return str(number)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
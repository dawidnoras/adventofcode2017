import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Rule:
    def __init__(self, rule_id):
        self.rule_id = rule_id
        self.alternative_sub_rules = []
        self.value = None

    def __repr__(self):
        return '{' + str(self.rule_id) + '}'

    def set_alternative_sub_rules(self, sub_rules):
        # for y in sub_rules:
        #     for x in y:
        #         if x
        # # if self.rule_id == 8:
        # #     tab = [sub_rules[0][0]]
        # #     for i in range(1, 30):
        # #         x = tab * i
        # #         self.alternative_sub_rules.append(x)
        # if self.rule_id == 11:
        #     for i in range(1, 30):
        #         x = [sub_rules[0][0]]
        #         for j in range(i - 1):
        #             x.append(sub_rules[0][0])
        #         for j in range(i - 1):
        #             x.append(sub_rules[0][1])
        #         x.append(sub_rules[0][1])
        #         self.alternative_sub_rules.append(x)
        # else:
        self.alternative_sub_rules = sub_rules

    def set_value(self, value):
        self.value = value

    def match(self, string):
        if self.value is not None:
            return string[0] == self.value, 1
        
        for alt in self.alternative_sub_rules:
            str_to_check = string
            for r in alt:
                matched, length = r.match(str_to_check)
                if matched:
                    str_to_check = str_to_check[length:]
                else:
                    break
            if matched:
                return True, len(string) - len(str_to_check)
        return False, 0
    
    def match_2(self, string):
        if self.value is not None:
            if len(string) == 0:
                return False, 0
            return string[0] == self.value, 1
        
        for alt in self.alternative_sub_rules:
            str_to_check = string
            for r in alt:
                # if r.rule_id == 11:
                #     print(11)
                matched, length = r.match_2(str_to_check)
                # if len(str_to_check) > 0:
                #     matched, length = r.match_2(str_to_check)
                #     # if not matched and r.rule_id in [11]:
                #     #     matched, length = True, 0
                # elif r is not self:
                #     matched, length = False, 0
                if matched:
                    str_to_check = str_to_check[length:]
                else:
                    break
            if matched:
                return True, len(string) - len(str_to_check)
        return False, 0
                
                
                

def parse_input(file):
    rule_pattern = re.compile('(\\d+): ([\\d \\|"ab]+)')
    rules = []
    data = []
    
    lines = file.readlines()
    # max_rule_id = max([int(line.split(':')[0]) for line in lines])
    
    state = 'rules'
    for line in lines:
        if state == 'rules':
            if line == '\n':
                state = 'data'
                continue
            (rule_id, sub_rule_str) = rule_pattern.match(line).groups()
            rule = Rule(int(rule_id))
            if sub_rule_str in ['"a"', '"b"']:
                rule.set_value(sub_rule_str[1])
            rules.append(rule)
        elif state == 'data':
            data.append(line.rstrip('\n'))
               
    rules.sort(key= lambda x: x.rule_id)
      
    or_rule_pattern = re.compile('(\\d+): ([\\d \\|"ab]+)') 
    for line in lines: 
        if '"a"' in line or '"b"' in line:
            continue
        if line == '\n':
            break
        
        (rule_id, sub_rule_str) = rule_pattern.match(line).groups()
        sub_rule_strs = sub_rule_str.split('|')
        alternative_sub_rules = []
        for sub_rule_str in sub_rule_strs:
            sub_rule_ids = sub_rule_str.strip(' ').split(' ')
            sub_rules = []
            for sub_rule_id in sub_rule_ids:
                int_id = int(sub_rule_id)
                sub_rule = next(filter(lambda x: x.rule_id == int_id, rules))
                sub_rules.append(sub_rule)
            alternative_sub_rules.append(sub_rules)
        rules[int(rule_id)].set_alternative_sub_rules(alternative_sub_rules)
    return rules, data


@aoc.timer
def run_challenge_one(challenge_input):
    rules, data = challenge_input
    
    s = 0
    for entry in data:
        match, length = rules[0].match(entry)
        if match and len(entry) == length:
            s = s + 1
        
    return str(s)


@aoc.timer
def run_challenge_two(challenge_input):
    rules, data = challenge_input
    
    start_rule = Rule(0)
    for i in range(1, 20):
        for j in range(1, 20):
            alt_sub_rules = []
            for di in range(i):
                alt_sub_rules.append(rules[8].alternative_sub_rules[0][0])
            for dj in range(j):
                alt_sub_rules.append(rules[11].alternative_sub_rules[0][0])
            for dj in range(j):
                alt_sub_rules.append(rules[11].alternative_sub_rules[0][1])
            start_rule.alternative_sub_rules.append(alt_sub_rules)
    rules[0] = start_rule     
    
    
    s = 0
    for entry in data:
        match, length = rules[0].match_2(entry)
        if match and length != len(entry):
            print(entry)
        if match and len(entry) == length:
            s = s + 1
        
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
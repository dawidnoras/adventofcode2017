import sys
import os
from typing import List
import re
import itertools
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    return [int(c) for c in file.readline().rstrip('\n')]


@aoc.timer
def run_challenge_one(challenge_input):
    cups = challenge_input
    length = len(cups)
    
    current_cup = cups[0]
    current_cup_index = cups.index(current_cup)
    for _ in range(100):   
        # take 3 cups out
        next_cups = []
        for i in range(1, 4):
            next_cups.append(cups[(current_cup_index + i) % length])
        for cup in next_cups:
            cups.remove(cup)
            
        # find destination cup
        destination_cup = current_cup
        while True:
            destination_cup = destination_cup - 1
            if destination_cup <= 0:
                destination_cup = length
            if next(filter(lambda x: x == destination_cup, next_cups), None) is not None:
                continue
            destination_cup_index = cups.index(destination_cup)
            for i, cup in enumerate(next_cups):
                cups.insert(destination_cup_index + i + 1, cup)
            break
        
        # next current cup
        current_cup_index = cups.index(current_cup)
        current_cup = cups[(current_cup_index + 1) % length]
        current_cup_index = cups.index(current_cup)
            
    # by hand
    return str(0)


class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

    def __repr__(self):
        return '{' + str(self.value) + '}'

@aoc.timer
def run_challenge_two(challenge_input):
    cups = challenge_input
    
    iterations = 10000000
    head = Node(cups[0])
    prev = head
    for i in itertools.chain(cups[1:], range(max(cups) + 1, 1000001)):
        node = Node(i)
        prev.next = node
        prev = node
        if i % 1000 == 0:
            print(i)
    prev.next = head
    length = 1000000
    
    node_dict = {}
    node = head
    for _ in range(1000000):
        node_dict[node.value] = node
        node = node.next
    
    current = head
    for move_index in range(iterations):   
        # take 3 cups out
        removed = current.next
        current.next = removed.next.next.next
            
        # find destination cup
        destination_value = current.value
        check_tab = [
            removed.value,
            removed.next.value,
            removed.next.next.value
        ]
        while True:
            destination_value = destination_value - 1
            if destination_value <= 0:
                destination_value = length
            if destination_value in check_tab:
                continue
            destination = node_dict[destination_value]
            next_one = destination.next
            destination.next = removed
            removed.next.next.next = next_one
            break
        
        # next current cup
        current = current.next
        if move_index % 1000 == 0:
            print(move_index)

    one_node = node_dict[1]
    result = one_node.next.value * one_node.next.next.value
    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
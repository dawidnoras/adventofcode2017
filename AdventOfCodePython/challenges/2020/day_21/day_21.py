import sys
import os
from typing import List
import re
import operator
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Alergen:
    def __init__(self, name):
        self.name = name
        self.potential_ingredients = []
    
    def __repr__(self):
        return '{Alergen: ' + str(self.name) + '}'
    

class Ingredient:
    def __init__(self, name):
        self.name = name
        self.potential_alergens = []
        self.alergen = None
    
    def __repr__(self):
        return '{Ingredient: ' + str(self.name) + '}'
    
    def __lt__(self, other):
        return self.alergen.name < other.alergen.name


class Food:
    def __init__(self, ingredients, alergens):
        self.ingredients = ingredients
        self.alergens = alergens


def parse_input(file):
    pattern = re.compile('([\\w ]+) \\(contains ([[\\w ,]+)\\)')
    foods = []
    
    lines = file.readlines()
    ingredients = []
    alergens = []
    for line in lines:
        (ingredients_str, alergens_str) = pattern.match(line).groups()
        for ingredient_str in ingredients_str.split(' '):
            if next(filter(lambda x: x.name == ingredient_str, ingredients), None) is None:
                ingredients.append(Ingredient(ingredient_str))
        for alergen_str in alergens_str.split(', '):
            if next(filter(lambda x: x.name == alergen_str, alergens), None) is None:
                alergens.append(Alergen(alergen_str))
                
        
    for line in lines:
        (ingredients_str, alergens_str) = pattern.match(line).groups()
        food_ingredients = []
        for ingredient_str in ingredients_str.split(' '):
            food_ingredients.append(next(filter(lambda x: x.name == ingredient_str, ingredients)))
        food_alergens = []
        for alergen_str in alergens_str.split(', '):
            food_alergens.append(next(filter(lambda x: x.name == alergen_str, alergens)))
        foods.append(Food(food_ingredients, food_alergens))
    return foods, ingredients, alergens


@aoc.timer
def run_challenge_one(challenge_input):
    foods, ingredients, alergens = challenge_input
    
    for alergen in alergens:
        foods_with_alergen = filter(lambda x: alergen in x.alergens, foods)
        potential_ingredients = next(foods_with_alergen).ingredients.copy()
        to_remove = []
        for food_with_alergen in foods_with_alergen:
            for potential_ingredient in potential_ingredients:
                if potential_ingredient not in food_with_alergen.ingredients and potential_ingredient not in to_remove:
                    to_remove.append(potential_ingredient)
        for x in to_remove:
            potential_ingredients.remove(x)
        alergen.potential_ingredients = potential_ingredients.copy()

    for alergen in alergens:
        for ingredient in alergen.potential_ingredients:
            if alergen not in ingredient.potential_alergens:
                ingredient.potential_alergens.append(alergen)
    
    alergen_free_ingredients = list(filter(lambda x: len(x.potential_alergens) == 0, ingredients))
    counter = 0
    for food in foods:
        for ingredient in food.ingredients:
            if ingredient in alergen_free_ingredients:
                counter = counter + 1
            
    return str(counter)


@aoc.timer
def run_challenge_two(challenge_input):
    foods, ingredients, alergens = challenge_input
    
    for alergen in alergens:
        foods_with_alergen = filter(lambda x: alergen in x.alergens, foods)
        potential_ingredients = next(foods_with_alergen).ingredients.copy()
        to_remove = []
        for food_with_alergen in foods_with_alergen:
            for potential_ingredient in potential_ingredients:
                if potential_ingredient not in food_with_alergen.ingredients and potential_ingredient not in to_remove:
                    to_remove.append(potential_ingredient)
        for x in to_remove:
            potential_ingredients.remove(x)
        alergen.potential_ingredients = potential_ingredients.copy()

    for alergen in alergens:
        for ingredient in alergen.potential_ingredients:
            if alergen not in ingredient.potential_alergens:
                ingredient.potential_alergens.append(alergen)
    
    done_alergens = []
    
    while True:
        sure_alergen = next(filter(lambda x: len(x.potential_ingredients) == 1 and x not in done_alergens, alergens), None)
        if sure_alergen is None:
            break
        
        for alergen in alergens:
            if alergen == sure_alergen:
                continue
            if sure_alergen.potential_ingredients[0] in alergen.potential_ingredients:
                alergen.potential_ingredients.remove(sure_alergen.potential_ingredients[0])
        sure_alergen.potential_ingredients[0].potential_alergens = [sure_alergen]         
        sure_alergen.ingredient = sure_alergen.potential_ingredients[0]
        sure_alergen.ingredient.alergen = sure_alergen
        
        done_alergens.append(sure_alergen)
    
    l = list(sorted(filter(lambda x: len(x.potential_alergens) > 0, ingredients)))
    result = ','.join([x.name for x in l])
     
    return result


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
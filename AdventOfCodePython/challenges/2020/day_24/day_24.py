import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

E = 'E'
SE = 'SE'
SW = 'SW'
W = 'W'
NW = 'NW'
NE = 'NE'

DIRECTIONS = [
    E,
    SE,
    SW,
    W,
    NW,
    NE
]

DIR_MAP = {
    E:  [1, -1, 0],
    SE: [0, -1, 1],
    SW:  [-1, 0, 1],
    W:  [-1, 1, 0],
    NW:  [0, 1, -1],
    NE:  [1, 0, -1],
}

def parse_input(file):
    lines = []
    
    for line in file:
        line = line.rstrip('\n')
        line = list(line)
        directions = []
        while len(line) > 0:
            c = line.pop(0)
            if c == 's':
                c = line.pop(0)
                if c == 'e':
                    directions.append(SE)
                elif c == 'w':
                    directions.append(SW)
                else:
                    print('error')
            elif c == 'n':
                c = line.pop(0)
                if c == 'e':
                    directions.append(NE)
                elif c == 'w':
                    directions.append(NW)
                else:
                    print('error')
            elif c == 'e':
                directions.append(E)
            elif c == 'w':
                directions.append(W)
            else:
                print('error')
        lines.append(directions)
                    
    return lines


def move(tile, direction):
    dir_tab = DIR_MAP[direction]
    for i in range(3):
        tile[i] = tile[i] + dir_tab[i]
    return tile


@aoc.timer
def run_challenge_one(challenge_input):
    black_tiles = {}
    
    for line in challenge_input:
        tile = [0, 0, 0]
        for direction in line:
            move(tile, direction)
        tup = tuple(tile)
        if tup in black_tiles:
            del black_tiles[tup]
        else:
            black_tiles[tup] = tup
    
    result = len(black_tiles)
    return str(result)


def move_new(tile, direction):
    dir_tab = DIR_MAP[direction]
    result = [0, 0, 0]
    for i in range(3):
        result[i] = tile[i] + dir_tab[i]
    return result


BLACK = 1
WHITE = 0


def get_number_of_black_neighbours(tile, black_tiles):
    counter = 0
    for direction in DIRECTIONS:
        new_tile = move_new(tile, direction)
        new_tile = tuple(new_tile)
        if new_tile in black_tiles:
            counter = counter + 1
    return counter


@aoc.timer
def run_challenge_two(challenge_input):
    black_tiles = {}
    
    for line in challenge_input:
        tile = [0, 0, 0]
        for direction in line:
            move(tile, direction)
        tup = tuple(tile)
        if tup in black_tiles:
            del black_tiles[tup]
        else:
            black_tiles[tup] = tup
    
    for _ in range(100):
        tiles_to_check = {}
        coppied = black_tiles.copy()
        for tile in black_tiles:
            tiles_to_check[tile] = BLACK
        for tile in black_tiles:
            for direction in DIRECTIONS:
                new_tile = move_new(tile, direction)
                new_tile = tuple(new_tile)
                if new_tile not in tiles_to_check:
                    tiles_to_check[new_tile] = WHITE
                    
        for tile in tiles_to_check:
            if tiles_to_check[tile] == BLACK:
                n = get_number_of_black_neighbours(tile, black_tiles)
                if n == 0 or n > 2:
                    del coppied[tile]
            else:
                n = get_number_of_black_neighbours(tile, black_tiles)
                if n == 2:
                    coppied[tile] = tile
        black_tiles = coppied
            
     
    return str(len(black_tiles))


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
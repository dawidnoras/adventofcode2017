import sys
import os
from typing import List
from copy import deepcopy
import itertools
import re
import math
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

SIZE = 10

DIRECTION = [
    'E',
    'N',
    'W',
    'S'
]

FILE_PATH = 'image.txt'
    
class Tile:
    def __init__(self, tile_id, tab):
        self.tile_id = tile_id
        self.tab = tab
        self.permutations = [self.tab]
        for i in range(3):
            self.tab = deepcopy(self.tab)
            self.rotate(right=True)
            self.permutations.append(self.tab)
        self.tab = deepcopy(self.tab)
        self.flip(horizontal=True)
        self.permutations.append(self.tab)
        for i in range(3):
            self.tab = deepcopy(self.tab)
            self.rotate(right=True)
            self.permutations.append(self.tab)
        self.tab = tab
        
        self.valid_tiles = {}
        for i in range(len(self.permutations)):
            self.valid_tiles[i] = {}
            for d in DIRECTION:
                self.valid_tiles[i][d] = []

    def __repr__(self):
        return '{' + str(self.tile_id) + '}'
              
    def rotate(self, right):
        copy_tab = deepcopy(self.tab)
        if right:
            for y, line in enumerate(copy_tab):
                for x, char in enumerate(line):
                    self.tab[x][SIZE - y - 1] = char
        else:
            for y, line in enumerate(copy_tab):
                for x, char in enumerate(line):
                    self.tab[SIZE - x - 1][y] = char
    
    def flip(self, horizontal):
        if horizontal:
            for y in range(int(SIZE / 2)):
                for x in range(SIZE):
                    temp = self.tab[y][x]
                    self.tab[y][x] = self.tab[SIZE - y - 1][x]
                    self.tab[SIZE - y - 1][x] = temp
        else:
            for y in range(SIZE):
                for x in range(int(SIZE / 2)):
                    temp = self.tab[y][x]
                    self.tab[y][x] = self.tab[y][SIZE - x - 1]
                    self.tab[y][SIZE - x - 1] = temp

    def print(self):
        for y in self.tab:
            print(' '.join(y))
        
        
def parse_input(file):
    tiles = []
    
    # early out for part 2:
    if os.path.exists(FILE_PATH):
        return tiles
    
    state = 'header'
    for line in file:
        if state == 'header':
            tile_id = line.split(' ')[1][:-2]
            tab = []
            state = 'tile'
        elif state == 'tile':
            if line == '\n':
                tiles.append(Tile(int(tile_id), tab))
                state = 'header'
            else:
                tab.append(list(line.rstrip('\n')))
    # add last tile
    tiles.append(Tile(int(tile_id), tab))

    return tiles

def check_sides(tab1, tab2, d):
    if d == 'E':
        for i in range(SIZE):
            if tab1[i][SIZE - 1] != tab2[i][0]:
                return False
    elif d == 'N':
        for i in range(SIZE):
            if tab1[0][i] != tab2[SIZE - 1][i]:
                return False
    elif d == 'W':
        for i in range(SIZE):
            if tab1[i][0] != tab2[i][SIZE - 1]:
                return False
    elif d == 'S':
        for i in range(SIZE):
            if tab1[SIZE - 1][i] != tab2[0][i]:
                return False
    
    return True

@aoc.timer
def run_challenge_one(challenge_input):
    tiles = challenge_input
    
    # assume first tile is placed in correct orientation
    for tile_combination in itertools.combinations(tiles, 2):
        tile_1 = tile_combination[0]
        tile_2 = tile_combination[1]
        for p_index_1, permutation_1 in enumerate(tile_1.permutations):
            for d in DIRECTION:
                for p_index_2, permutation_2 in enumerate(tile_2.permutations):
                    if check_sides(permutation_1, permutation_2, d):
                        tile_1.valid_tiles[p_index_1][d].append({
                            'tile': tile_2,
                            'permutation_index': p_index_2,
                            'permutation': permutation_2
                        })
                        if d == 'N':
                            d_2 = 'S'
                        elif d == 'S':
                            d_2 = 'N'
                        elif d == 'W':
                            d_2 = 'E'
                        elif d == 'E':
                            d_2 = 'W'
                        tile_2.valid_tiles[p_index_2][d_2].append({
                            'tile': tile_1,
                            'permutation_index': p_index_1,
                            'permutation': permutation_1
                        })
    
    corner_tiles = []
    for tile in tiles:
        c = 0
        for d in tile.valid_tiles[0]:
            if len(tile.valid_tiles[0][d]) > 1:
                print(tile)
            if len(tile.valid_tiles[0][d]) == 0:
                c = c + 1
        if c == 2:
            corner_tiles.append(tile)
            
    s = 1
    for tile in corner_tiles:
        s = s * tile.tile_id
    
    return str(s)

def flip(tab, horizontal=True):
    height = len(tab)
    width = len(tab[0])
    copy_tab = deepcopy(tab)
    if horizontal:
        for y in range(int(height / 2)):
            for x in range(width):
                temp = copy_tab[y][x]
                copy_tab[y][x] = copy_tab[height - y - 1][x]
                copy_tab[height - y - 1][x] = temp
    else:
        for y in range(height):
            for x in range(int(width / 2)):
                temp = copy_tab[y][x]
                copy_tab[y][x] = copy_tab[y][width - x - 1]
                copy_tab[y][width - x - 1] = temp
    return copy_tab

@aoc.timer
def run_challenge_two(challenge_input):
    tiles = challenge_input
    
    if not os.path.exists(FILE_PATH):
        # assume first tile is placed in correct orientation
        for tile_combination in itertools.combinations(tiles, 2):
            tile_1 = tile_combination[0]
            tile_2 = tile_combination[1]
            for p_index_1, permutation_1 in enumerate(tile_1.permutations):
                for d in DIRECTION:
                    for p_index_2, permutation_2 in enumerate(tile_2.permutations):
                        if check_sides(permutation_1, permutation_2, d):
                            tile_1.valid_tiles[p_index_1][d].append({
                                'tile': tile_2,
                                'p_index': p_index_2,
                                'permutation': permutation_2
                            })
                            if d == 'N':
                                d_2 = 'S'
                            elif d == 'S':
                                d_2 = 'N'
                            elif d == 'W':
                                d_2 = 'E'
                            elif d == 'E':
                                d_2 = 'W'
                            tile_2.valid_tiles[p_index_2][d_2].append({
                                'tile': tile_1,
                                'p_index': p_index_1,
                                'permutation': permutation_1
                            })
        
        # find corner tiles
        corner_tiles = []
        for tile in tiles:
            c = 0
            for d in tile.valid_tiles[0]:
                if len(tile.valid_tiles[0][d]) == 0:
                    c = c + 1
            if c == 2:
                corner_tiles.append(tile)
        
        corner_tiles_valid_permutations = []
        # find top left corner valid permutations
        for corner_tile in corner_tiles:
            # for p_index, permutation in enumerate(corner_tile.permutations):
            for p_index in corner_tile.valid_tiles:
                valid_tiles = corner_tile.valid_tiles[p_index]
                if len(valid_tiles['N']) > 0 or len(valid_tiles['W']) > 0:
                    continue
                corner_tiles_valid_permutations.append({
                    'tile': corner_tile,
                    'p_index': p_index,
                    'permutation': corner_tile.permutations[p_index]
                })
        
        # try to find correct left top corner tile orientation
        image_width = int(math.sqrt(len(tiles)))
        for top_left_corner in corner_tiles_valid_permutations:
            image = [[top_left_corner]]
            top_left_corner_tile = top_left_corner['tile']
            counter = 1
            next_tile_entry = top_left_corner_tile.valid_tiles[top_left_corner['p_index']]['S']
            while len(next_tile_entry) > 0:
                next_tile = next_tile_entry[0]['tile']
                image.append([next_tile_entry[0]])
                next_tile_entry = next_tile.valid_tiles[next_tile_entry[0]['p_index']]['S']
                counter = counter + 1
            if counter != image_width:
                continue
            
            for i, line in enumerate(image):
                counter = 0
                first_tile_entry = line[0]
                first_tile = first_tile_entry['tile']
                next_tile_entry = first_tile.valid_tiles[first_tile_entry['p_index']]['E']
                while len(next_tile_entry) > 0:
                    next_tile = next_tile_entry[0]['tile']
                    image[i].append(next_tile_entry[0])
                    next_tile_entry = next_tile.valid_tiles[next_tile_entry[0]['p_index']]['E']
                    counter = counter + 1
            if counter != image_width:
                continue
            break
                
        # assemble image
        # start = 0
        # end = SIZE
        start = 1
        end = SIZE - 1
        composed_image = []
        for line in image:
            for y in range(start, end):
                composed_image_line = []
                for tile_entry in line:
                    tile = tile_entry['tile']
                    for x in range(start, end):
                        composed_image_line.append(tile_entry['permutation'][y][x])
                composed_image.append(composed_image_line)
            
        # test fit
        # for i in range(len(composed_image)):
        #     for j in reversed(range(1, 4)):
        #         composed_image[i].insert(j * 10, ' ')
        #      
        # for i, y in enumerate(composed_image):
        #     if i % 10 == 0:
        #         print()
        #     print('  '.join(y))
        
        # print assembled
        for i, y in enumerate(composed_image):
            print('  '.join(y))
          
        # store assembled
        with open(FILE_PATH, 'w') as f:
            for line in composed_image:
                f.write(''.join(line))
                f.write('\n')
    else:
        composed_image = []
        with open(FILE_PATH, 'r') as f:
            for line in f:
                composed_image.append([c for c in line.rstrip('\n')])
        
    # create all sea monster patterns
    sea_monster_pattern_str = [
        '                  # ',
        '#    ##    ##    ###',
        ' #  #  #  #  #  #   '
    ]
    sea_monster_pattern = [[c for c in line] for line in sea_monster_pattern_str]
    sea_monster_patterns = [sea_monster_pattern]
    
    sea_monster_pattern_copy = deepcopy(sea_monster_pattern)  
    new_pattern = []
    for y, line in enumerate(sea_monster_pattern_copy):
        for x, char in enumerate(line):
            if y == 0:
                new_pattern.append([char])
            else:
                new_pattern[x].append(char)
    sea_monster_patterns.append(new_pattern)
    
    flipped = flip(sea_monster_pattern, horizontal=True)
    sea_monster_patterns.append(flipped)
    sea_monster_patterns.append(flip(flipped, horizontal=False))
    sea_monster_patterns.append(flip(sea_monster_pattern, horizontal=False))
    flipped = flip(new_pattern, horizontal=True)
    sea_monster_patterns.append(flipped)
    sea_monster_patterns.append(flip(flipped, horizontal=False))
    sea_monster_patterns.append(flip(new_pattern, horizontal=False))
    
    # pattern to dict
    dict_patterns = []
    for sea_monster_pattern in sea_monster_patterns:
        width = len(sea_monster_pattern[0])
        height = len(sea_monster_pattern)
        dict_pattern = {
            'pattern': sea_monster_pattern,
            'dicted': []
        }
        for y in range(height):
            for x in range(width):
                if sea_monster_pattern[y][x] == '#':
                    dict_pattern['dicted'].append({
                        'x': x,
                        'y': y
                    })
        dict_patterns.append(dict_pattern)
                
    # search for sea monsters
    im_width = len(composed_image[0])
    im_height = len(composed_image)
    for dict_pattern in dict_patterns:
        width = len(dict_pattern['pattern'][0])
        height = len(dict_pattern['pattern'])
        for im_y in range(im_height - height):
            for im_x in range(im_width - width):
                # check if see monster is here
                found = True
                for offset in dict_pattern['dicted']:
                    x = im_x + offset['x']
                    y = im_y + offset['y']
                    if composed_image[y][x] not in ['#', 'O']:
                        found = False
                        break
                if found:
                    for offset in dict_pattern['dicted']:
                        x = im_x + offset['x']
                        y = im_y + offset['y']
                        composed_image[y][x] = 'O'
    
    counter = 0
    for i, y in enumerate(composed_image):
        for c in y:
            if c == '#':
                counter = counter + 1
        print('  '.join(y))
    
    return str(counter)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
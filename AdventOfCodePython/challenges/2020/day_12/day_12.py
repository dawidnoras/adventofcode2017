import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


N = [0, 1]
S = [0, -1]
E = [1, 0]
W = [-1, 0]

DIRECTIONS = [
    E,
    N,
    W,
    S
]


def turn(current_direction, turn_direction, degrees):
    steps = int(degrees / 90)
    index = DIRECTIONS.index(current_direction)
    index = index + steps if turn_direction == 'L' else index - steps
    if index < 0:
        index = index + len(DIRECTIONS)
    if index > 0:
        index = index - len(DIRECTIONS)
    return DIRECTIONS[index]


def parse_input(file):
    p = re.compile('([NSEWLRF])(\\d+)')
    input_list = []
    for line in file:
        (action, value) = p.match(line).groups()
        input_list.append({
            'action': action,
            'value': int(value)
        })
    return input_list


@aoc.timer
def run_challenge_one(challenge_input):
    actions = challenge_input
    
    ship_direction = E
    x_diff = 0
    y_diff = 0
    for action in actions:
        if action['action'] == 'N':
            y_diff = y_diff + action['value']
        elif action['action'] == 'S':
            y_diff = y_diff - action['value']
        elif action['action'] == 'E':
            x_diff = x_diff + action['value']
        elif action['action'] == 'W':
            x_diff = x_diff - action['value']
            
        elif action['action'] == 'L':
            ship_direction = turn(ship_direction, 'L', action['value'])
        elif action['action'] == 'R':
            ship_direction = turn(ship_direction, 'R', action['value'])
            
        elif action['action'] == 'F':
            x_diff = x_diff + action['value'] * ship_direction[0]
            y_diff = y_diff + action['value'] * ship_direction[1]
            
    result = abs(x_diff) + abs(y_diff)
    return str(result)


def turn_waypoint(current_direction, turn_direction, degrees):
    steps = int(degrees / 90)
    if steps == 3:
        steps = 1
        turn_direction = 'L' if turn_direction == 'R' else 'R'
    if steps == 2:
        direction = [-current_direction[0], -current_direction[1]]
        return direction

    if turn_direction == 'R':
        direction = [current_direction[1], -current_direction[0]]
        return direction
    if turn_direction == 'L':
        direction = [-current_direction[1], current_direction[0]]
        return direction
    
    
@aoc.timer
def run_challenge_two(challenge_input):
    actions = challenge_input
    
    waypoint = [10, 1]
    x_diff = 0
    y_diff = 0
    for action in actions:
        if action['action'] == 'N':
            waypoint[1] = waypoint[1] + action['value']
        elif action['action'] == 'S':
            waypoint[1] = waypoint[1] - action['value']
        elif action['action'] == 'E':
            waypoint[0] = waypoint[0] + action['value']
        elif action['action'] == 'W':
            waypoint[0] = waypoint[0] - action['value']
            
        elif action['action'] == 'L':
            waypoint = turn_waypoint(waypoint, 'L', action['value'])
        elif action['action'] == 'R':
            waypoint = turn_waypoint(waypoint, 'R', action['value'])
            
        elif action['action'] == 'F':
            x_diff = x_diff + action['value'] * waypoint[0]
            y_diff = y_diff + action['value'] * waypoint[1]
            
    result = abs(x_diff) + abs(y_diff)
    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
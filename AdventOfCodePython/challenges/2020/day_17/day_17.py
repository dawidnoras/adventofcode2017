import sys
import os
from typing import List
import re
import copy
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


INACTIVE = '.'
ACTIVE = '#'


def parse_input(file):
    result = []
    lines = [line.rstrip('\n') for line in file]
    for line in lines:
        chars = []
        for char in line:
            chars.append(char)
        result.append(chars)
    return result


def number_of_adjenced_active_cubes_ch_one(tab, pos_x, pos_y, pos_z, width, height, depth):
    count = 0
    for z in range(-1, 2):
        if 0 <= pos_z + z < depth:
            for y in range(-1, 2):
                if 0 <= pos_y + y < height:
                    for x in range(-1, 2):
                        if 0 <= pos_x + x < width and \
                                not (x == 0 and y == 0 and z == 0) and \
                                tab[pos_z + z][pos_y + y][pos_x + x] == ACTIVE:
                            count = count + 1
    return count


def print_tab(tab, step):
    os.system('cls')
    print()
    print('-----{}----'.format(step))
    for i, z in enumerate(tab):
        print('z={}'.format(i))
        for y in z:
            print(y)


def create_tab(max_z, max_y, max_x):
    tab = []
    for _ in range(max_z):
        z_tab = []
        for _ in range(max_y):
            y_tab = []
            for _ in range(max_x):
                y_tab.append(INACTIVE)
            z_tab.append(y_tab)
        tab.append(z_tab)
    return tab
    

@aoc.timer
def run_challenge_one(challenge_input):
    max_xy_dimension = len(challenge_input) + 6 * 2
    max_z_dimension = 6 * 2 + 1
    
    initial_z = 6
    initial_x = 6
    initial_y = 6
    span_z = 1 + 2
    span_x = len(challenge_input) + 2
    span_y = len(challenge_input) + 2
    tab = create_tab(max_z_dimension, max_xy_dimension, max_xy_dimension)
                
    for y, line in enumerate(challenge_input):
        propert_y = initial_y + y
        for x, char in enumerate(line):
            proper_x = initial_x + x
            tab[initial_z][propert_y][proper_x] = char
    
    print_tab(tab, 0)

    for i in range(6):
        prev_tab = tab
        tab = copy.deepcopy(prev_tab)
        
        for z in range(max_z_dimension):
            for y in range(max_xy_dimension):
                for x in range(max_xy_dimension):
                    number_of_adjenced_active_cubes = number_of_adjenced_active_cubes_ch_one(prev_tab, x, y, z, max_xy_dimension, max_xy_dimension, max_z_dimension)
                    if prev_tab[z][y][x] == ACTIVE:
                        if number_of_adjenced_active_cubes not in [2, 3]: 
                            tab[z][y][x] = INACTIVE
                    else:
                        if number_of_adjenced_active_cubes == 3:
                            tab[z][y][x] = ACTIVE                
        print_tab(tab, i)
        
        
    s = 0
    for z in tab:
        for y in z:
            for x in y:
                if x == ACTIVE:
                    s = s + 1
    return str(s)


def number_of_adjenced_active_cubes_ch_two(tab, pos_x, pos_y, pos_z, pos_w, width, height, depth, w_dimension):
    count = 0
    for w in range(-1, 2):
        if 0 <= pos_w + w < w_dimension:
            for z in range(-1, 2):
                if 0 <= pos_z + z < depth:
                    for y in range(-1, 2):
                        if 0 <= pos_y + y < height:
                            for x in range(-1, 2):
                                if 0 <= pos_x + x < width and \
                                        not (x == 0 and y == 0 and z == 0 and w == 0) and \
                                        tab[pos_w + w][pos_z + z][pos_y + y][pos_x + x] == ACTIVE:
                                    count = count + 1
    return count


def create_tab_two(max_w, max_z, max_y, max_x):
    tab = []
    for _ in range(max_w):
        w_tab = []
        for _ in range(max_z):
            z_tab = []
            for _ in range(max_y):
                y_tab = []
                for _ in range(max_x):
                    y_tab.append(INACTIVE)
                z_tab.append(y_tab)
            w_tab.append(z_tab)
        tab.append(w_tab)
    return tab

@aoc.timer
def depp_copy(tab):
    tab = copy.deepcopy(tab)


@aoc.timer
def run_challenge_two(challenge_input):
    max_xy_dimension = len(challenge_input) + 6 * 2
    max_z_dimension = 6 * 2 + 1
    
    initial_w = 6
    initial_z = 6
    initial_x = 6
    initial_y = 6
    span_z = 1 + 2
    span_x = len(challenge_input) + 2
    span_y = len(challenge_input) + 2
    tab = create_tab_two(max_z_dimension, max_z_dimension, max_xy_dimension, max_xy_dimension)
                
    for y, line in enumerate(challenge_input):
        propert_y = initial_y + y
        for x, char in enumerate(line):
            proper_x = initial_x + x
            tab[initial_w][initial_z][propert_y][proper_x] = char
    
    # print_tab(tab, 0)

    for i in range(6):
        prev_tab = tab
        depp_copy(tab)
        tab = copy.deepcopy(prev_tab)
        for w in range(max_z_dimension):
            for z in range(max_z_dimension):
                for y in range(max_xy_dimension):
                    for x in range(max_xy_dimension):
                        number_of_adjenced_active_cubes = number_of_adjenced_active_cubes_ch_two(prev_tab, x, y, z, w, max_xy_dimension, max_xy_dimension, max_z_dimension, max_z_dimension)
                        if prev_tab[w][z][y][x] == ACTIVE:
                            if number_of_adjenced_active_cubes not in [2, 3]: 
                                tab[w][z][y][x] = INACTIVE
                        else:
                            if number_of_adjenced_active_cubes == 3:
                                tab[w][z][y][x] = ACTIVE    
        print(i)            
        # print_tab(tab, i)
        
        
    s = 0
    for w in tab:
        for z in w:
            for y in z:
                for x in y:
                    if x == ACTIVE:
                        s = s + 1
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
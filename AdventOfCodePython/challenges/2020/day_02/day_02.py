import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Password:
    def __init__(self, text: str, min_number: int, max_number: int, char: str):
        self.text = text
        self.min = int(min_number)
        self.max = int(max_number)
        self.char = char
        
    def is_valid_challenge_one(self) -> bool:
        occurancies = self.text.count(self.char)
        return self.min <= occurancies <= self.max
    
    def is_valid_challenge_two(self) -> bool:
        is_at_min = self.text[self.min -1] == self.char
        is_at_max = self.text[self.max -1] == self.char
        return is_at_min != is_at_max


def parse_input(file) -> List[Password]:
    p = re.compile('(\\d+)-(\\d+) (\\w): (\\w+)')
    password_list = []
    for line in file:
        (min_number, max_number, char, password) = p.match(line).groups()
        password_list.append(Password(password, min_number, max_number, char))
    return password_list


@aoc.timer
def run_challenge_one(challenge_input: List[Password]):
    count = 0
    for password in challenge_input:
        if (password.is_valid_challenge_one()):
            count = count + 1
    return str(count)


@aoc.timer
def run_challenge_two(challenge_input: List[Password]):
    count = 0
    for password in challenge_input:
        if (password.is_valid_challenge_two()):
            count = count + 1
    return str(count)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import sys
import os
from typing import List
import re
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    input_list = []
    for line in file:
        input_list.append(int(line.rstrip('\n')))
    return input_list


@aoc.timer
def run_challenge_one(challenge_input):
    adapters = challenge_input
    adapters.sort()
    
    one_jolt_differencs = 0
    two_jolt_differences = 0
    three_jolt_differencs = 0
    
    jolts = 0
    for i in range(len(adapters)):
        adapter_jolts = adapters[i]
        if adapter_jolts - jolts == 1:
            one_jolt_differencs = one_jolt_differencs + 1
        if adapter_jolts - jolts == 2:
            two_jolt_differences = two_jolt_differences + 1
        elif adapter_jolts - jolts == 3:
            three_jolt_differencs = three_jolt_differencs + 1 
        jolts = adapter_jolts
        
    # device diff
    three_jolt_differencs = three_jolt_differencs + 1
    result = one_jolt_differencs * three_jolt_differencs
    return str(result)


@aoc.timer
def run_challenge_two(challenge_input):
    adapters = challenge_input
    adapters.sort()
    adapters.insert(0, 0)
    
    l = len(adapters)
    adapter_connections_count = []
    for i in range(len(adapters)):
        if i == 0:
            adapter_connections_count.append(1)
            continue
        s = 0
        if i > 0 and adapters[i] - adapters[i - 1] <= 3:
            s = s + adapter_connections_count[i - 1]
        if i > 1 and adapters[i] - adapters[i - 2] <= 3:
            s = s +adapter_connections_count[i - 2]
        if i > 2 and adapters[i] - adapters[i - 3] <= 3:
            s = s + adapter_connections_count[i - 3]
        adapter_connections_count.append(s)
    
    return str(adapter_connections_count[l - 1])


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
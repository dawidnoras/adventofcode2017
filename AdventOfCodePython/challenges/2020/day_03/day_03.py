import sys
import os
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    lines = [line.rstrip('\n') for line in file.readlines()]
    return lines


@aoc.timer
def run_challenge_one(challenge_input: List[str]):
    forest = challenge_input
    width = len(forest[0])
    height = len(forest)
    
    x = 0
    y = 0
    tree_count = 0
    step = [3, 1]
    while y < height:
        if x >= width:
            x = x % width
        
        if forest[y][x] == '#':
            tree_count = tree_count + 1
        
        x = x + step[0]
        y = y + step[1]

    return str(tree_count)


@aoc.timer
def run_challenge_two(challenge_input: List[str]):
    forest = challenge_input
    width = len(forest[0])
    height = len(forest)
    
    steps = [
        [1, 1],
        [3, 1],
        [5, 1],
        [7, 1],
        [1, 2],
    ]
    
    result = 0
    for step in steps:
        x = 0
        y = 0
        tree_count = 0
        while y < height:
            if x >= width:
                x = x % width
            
            if forest[y][x] == '#':
                tree_count = tree_count + 1
            
            x = x + step[0]
            y = y + step[1]
        
        if result == 0:
            result = tree_count
        else:
            result = result * tree_count

    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
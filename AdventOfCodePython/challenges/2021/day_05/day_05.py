import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

class Segment():
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.is_diagonal = start[0] != end[0] and start[1] != end[1]

    def get_covered_points_not_diagonal(self):
        if self.is_diagonal:
            return

        x_step = 1 if self.start[0] <= self.end[0] else -1
        y_step = 1 if self.start[1] <= self.end[1] else -1

        for x in range(self.start[0], self.end[0] + x_step, x_step):
            for y in range(self.start[1], self.end[1] + y_step, y_step):
                yield {
                    'x': x,
                    'y': y
                }

    def get_covered_points_include_diagonal(self):
        x_step = 1 if self.start[0] <= self.end[0] else -1
        y_step = 1 if self.start[1] <= self.end[1] else -1

        if not self.is_diagonal:
            for x in range(self.start[0], self.end[0] + x_step, x_step):
                for y in range(self.start[1], self.end[1] + y_step, y_step):
                    yield {
                        'x': x,
                        'y': y
                    }
        else:
            y = self.start[1]
            for x in range(self.start[0], self.end[0] + x_step, x_step):
                yield {
                    'x': x,
                    'y': y
                }
                y = y + y_step

    def __repr__(self) -> str:
        return 'Segment: {},{} -> {},{}'.format(self.start[0], self.start[1], self.end[0], self.end[1])


def parse_input(file) -> List[Segment]:
    patern_segment = re.compile('(\\d+),(\\d+) -> (\\d+),(\\d+)')
    
    segments = []
    for line in file.readlines():
        match = patern_segment.match(line)
        (x1, y1, x2, y2) = match.groups()
        segments.append(Segment([int(x1), int(y1)], [int(x2), int(y2)]))

    return segments


@aoc.timer
def run_challenge_one(challenge_input: List[Segment]) -> str:
    covered_points = {}
    for segment in challenge_input:
        if segment.is_diagonal:
            continue
        for point in segment.get_covered_points_not_diagonal():
            immutable_key = '{},{}'.format(point['x'], point['y'])
            covered_point_value = covered_points.get(immutable_key, None)
            if covered_point_value is None:
                covered_points[immutable_key] = 1
            else:
                covered_points[immutable_key] = covered_point_value + 1
                
    counter = 0
    for key in covered_points:
        if covered_points[key] > 1:
            counter = counter + 1
    return str(counter)


@aoc.timer
def run_challenge_two(challenge_input: List[Segment]) -> str:
    covered_points = {}
    for segment in challenge_input:
        l = list(segment.get_covered_points_include_diagonal())
        for point in segment.get_covered_points_include_diagonal():
            immutable_key = '{},{}'.format(point['x'], point['y'])
            covered_point_value = covered_points.get(immutable_key, None)
            if covered_point_value is None:
                covered_points[immutable_key] = 1
            else:
                covered_points[immutable_key] = covered_point_value + 1
                
    counter = 0
    for key in covered_points:
        if covered_points[key] > 1:
            counter = counter + 1
    return str(counter)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import itertools
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    return [int(line) for line in file]


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    sum = 0
    for i in range(len(challenge_input) - 1):
        if challenge_input[i] < challenge_input[i + 1]:
            sum = sum + 1
    return str(sum)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    sum = 0
    last_sum = challenge_input[0] + challenge_input[1] + challenge_input[2]
    for i in range(len(challenge_input) - 3):
        new_sum = challenge_input[i + 1] + challenge_input[i + 2] + challenge_input[i + 3]
        if last_sum < new_sum:
            sum = sum + 1
        last_sum = new_sum
    return str(sum)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
from collections import deque
import statistics
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

def parse_input(file) -> List[List[int]]:
    result = []
    for line in file.readlines():
        line = line.rstrip('\n')
        result.append([int(c) for c in line])
    return result


MAKE_UNIQUE_KEY = lambda x, y: '{},{}'.format(x, y)


LEN_X = None
LEN_Y = None


def get_adjenced(x, y):
    for dx in range(-1, 2, 1):
        for dy in range(-1, 2, 1):
            nx = x + dx
            ny = y + dy
            if nx < 0 or nx >= LEN_X or ny < 0 or ny >= LEN_Y:
                continue
            if dx == 0 and dy == 0:
                continue
            yield nx, ny


@aoc.timer
def run_challenge_one(challenge_input: List[List[int]]) -> str:
    global LEN_X
    global LEN_Y
    LEN_Y = len(challenge_input)
    LEN_X = len(challenge_input[0])

    flash_counter = 0
    for i in range(100):
        already_flashed = set()
        to_check = []
        for y in range(LEN_Y):
            for x in range(LEN_X):
                challenge_input[y][x] = challenge_input[y][x] + 1
                if challenge_input[y][x] == 10:
                    already_flashed.add(MAKE_UNIQUE_KEY(x, y))
                    challenge_input[y][x] = 0
                    flash_counter = flash_counter + 1
                    for ax, ay in get_adjenced(x, y):
                        to_check.append([ax, ay])

        while len(to_check) > 0:
            new_to_check = []
            for check in to_check:
                x, y = check[0], check[1]
                unique_key = MAKE_UNIQUE_KEY(x, y)
                if unique_key in already_flashed:
                    continue
                challenge_input[y][x] = challenge_input[y][x] + 1
                if challenge_input[y][x] >= 10:
                    already_flashed.add(unique_key)
                    challenge_input[y][x] = 0
                    flash_counter = flash_counter + 1
                    for ax, ay in get_adjenced(x, y):
                        if MAKE_UNIQUE_KEY(ax, ay) not in already_flashed:
                            new_to_check.append([ax, ay])
            to_check = new_to_check


    return str(flash_counter)


@aoc.timer
def run_challenge_two(challenge_input: List[List[int]]) -> str:
    global LEN_X
    global LEN_Y
    LEN_Y = len(challenge_input)
    LEN_X = len(challenge_input[0])

    expected_flush_count = LEN_X * LEN_Y
    i = 0
    while True:
        flash_counter = 0
        already_flashed = set()
        to_check = []
        for y in range(LEN_Y):
            for x in range(LEN_X):
                challenge_input[y][x] = challenge_input[y][x] + 1
                if challenge_input[y][x] == 10:
                    already_flashed.add(MAKE_UNIQUE_KEY(x, y))
                    challenge_input[y][x] = 0
                    flash_counter = flash_counter + 1
                    for ax, ay in get_adjenced(x, y):
                        to_check.append([ax, ay])

        while len(to_check) > 0:
            new_to_check = []
            for check in to_check:
                x, y = check[0], check[1]
                unique_key = MAKE_UNIQUE_KEY(x, y)
                if unique_key in already_flashed:
                    continue
                challenge_input[y][x] = challenge_input[y][x] + 1
                if challenge_input[y][x] >= 10:
                    already_flashed.add(unique_key)
                    challenge_input[y][x] = 0
                    flash_counter = flash_counter + 1
                    for ax, ay in get_adjenced(x, y):
                        if MAKE_UNIQUE_KEY(ax, ay) not in already_flashed:
                            new_to_check.append([ax, ay])
            to_check = new_to_check
        i = i + 1
        if flash_counter == expected_flush_count:
            break

    return str(i)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
import statistics
import itertools
from copy import deepcopy

from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Node:
    def __init__(self, data=None, parent=None):
        self.left = None
        self.right = None
        self.parent = parent
        self.data = data
        self.level = -1

    
    def __repr__(self) -> str:
        return str(self.data) if self.data is not None else 'Pair'


def parse_input(file):
    numbers = []
    for line in file.readlines():
        line = line.rstrip('\n')

        trunk = None
        current_node = None
        for c in line:
            if c == '[':
                # add left node
                node = Node(parent=current_node)
                if trunk is None:
                    trunk = node
                else:
                    if current_node.left is None:
                        current_node.left = node
                    else:
                        current_node.right = node
                current_node = node
            elif c == ']':
                # go up the tree
                current_node = current_node.parent
                pass
            elif c == ',':
                # ignore
                pass
            else:
                # add leaf with value
                node = Node(data=int(c), parent=current_node)
                if current_node.left is None:
                    current_node.left = node
                else:
                    current_node.right = node
        numbers.append(trunk)
    return numbers


def add(num_1, num_2):
    trunk = Node(parent=None)
    num_1.parent = trunk
    num_2.parent = trunk
    trunk.left = deepcopy(num_1)
    trunk.right = deepcopy(num_2)
    return trunk


def traverse_literals(num, level):
    if num.data is not None:
        num.level = level
        yield num
    if num.left is not None:
        for n in traverse_literals(num.left, level + 1):
            yield n
    if num.right is not None:
        for n in traverse_literals(num.right, level + 1):
            yield n


def reduce(num):
    while True:
        candidat_for_split = None
        # explode
        prev_node = None
        exploding_pair = None
        right_exploding_node = None
        for node in traverse_literals(num, 0):
            if right_exploding_node is not None:
                if node is right_exploding_node:
                    continue
                node.data = node.data + right_exploding_node.data
                break
            if node.level == 5:
                exploding_pair = node.parent
                left = node
                right_exploding_node = exploding_pair.right
                if prev_node is not None:
                    prev_node.data = prev_node.data + left.data
            prev_node = node

            if node.data is not None and node.data >= 10 and \
                candidat_for_split is None:
                candidat_for_split = node

        if exploding_pair is not None:
            # exploded
            parent = right_exploding_node.parent.parent
            if parent.left is exploding_pair:
                parent.left = Node(data=0, parent=parent)
            else:
                parent.right = Node(data=0, parent=parent)
            continue
        # split 
        if candidat_for_split is not None:
            new_left = Node(data=candidat_for_split.data // 2, parent=candidat_for_split)
            new_right = Node(data=candidat_for_split.data - new_left.data, parent=candidat_for_split)
            candidat_for_split.left = new_left
            candidat_for_split.right = new_right
            candidat_for_split.data = None
            continue
        break


def calculate_magnitude(num):
    s = 0
    left = 0
    if num.left.data is not None:
        left = num.left.data
    else:
        left = calculate_magnitude(num.left)

    right = 0
    if num.right.data is not None:
        right = num.right.data
    else:
        right = calculate_magnitude(num.right)

    return 3 * left + 2 * right


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    num = None
    for n in challenge_input:
        if num is None:
            num = n
            continue
        num = add(num, n)
        reduce(num)
    return str(calculate_magnitude(num))



@aoc.timer
def run_challenge_two(challenge_input) -> str:
    max_magnitude = 0
    for a, b in itertools.permutations(challenge_input, 2):
        num = add(a, b)
        reduce(num)
        magnitude = calculate_magnitude(num)
        if magnitude > max_magnitude:
            max_magnitude = magnitude
    return str(max_magnitude)



if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
import statistics
import math
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

def parse_input(file) -> List[int]:
    return [int(num) for num in file.read().rstrip('\n').split(',')]


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    median = statistics.median(challenge_input)
    return str(sum([abs(distance - median) for distance in challenge_input]))


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    mean = math.floor(statistics.mean(challenge_input))
    triangular_number = lambda x: x * (x + 1) / 2
    return str(sum([triangular_number(abs(distance - mean)) for distance in challenge_input]))


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_ONE)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
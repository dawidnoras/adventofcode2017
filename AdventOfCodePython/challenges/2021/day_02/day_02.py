import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    result = [] 

    pattern = re.compile('^(\\w+) (\\d+)')
    for line in file.readlines():
        line = line.rstrip('\n')
        match = pattern.match(line)
        if match is not None:
            (command, value) = match.groups()
            result.append({
                'command': command,
                'value': int(value)
            })

    return result


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    x = 0
    y = 0
    for command in challenge_input:
        command_name = command['command']
        if command_name == 'forward':
            x = x + command['value']
        elif command_name == 'down':
            y = y + command['value']
        elif command_name == 'up':
            y = y - command['value']  
    return str(x * y)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    horizontal = 0
    depth = 0
    aim = 0

    for command in challenge_input:
        command_name = command['command']
        if command_name == 'forward':
            horizontal = horizontal + command['value']
            depth = depth + aim * command['value']
        elif command_name == 'down':
            aim = aim + command['value']
        elif command_name == 'up':
            aim = aim - command['value'] 

    return str(horizontal * depth)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
from collections import deque
import statistics
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


MAKE_UNIQUE_KEY = lambda x, y: '{}{}'.format(x, y)


class Part():
    def __init__(self, letter) -> None:
        self.letter = letter
        self.next = None


def parse_input(file):
    template = file.readline().rstrip('\n')

    pattern = re.compile('^(\\w+) -> (\\w+)')
    rules = {}
    for line in file.readlines():
        match = pattern.match(line)
        if match is not None:
            (left, right) = match.groups()
            rules[left] = right
    polymer = None
    polymer_end = None
    for c in template:
        if polymer is None:
            polymer = Part(c)
            polymer_end = polymer
        else:
            polymer_end.next = Part(c)
            polymer_end = polymer_end.next

    return polymer, rules


def print_polymer(polymer):
    letter = polymer
    s = letter.letter
    while letter.next is not None:
        s = s + letter.next.letter
        letter = letter.next
    print(s)


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    polymer, rules = challenge_input
    print_polymer(polymer)

    scores = {}
    pointer = polymer
    while pointer.next is not None:
        score = scores.get(pointer.letter, 0)
        scores[pointer.letter] = score + 1
        pointer = pointer.next
    score = scores.get(pointer.letter, 0)
    scores[pointer.letter] = score + 1

    for i in range(10):
        pointer = polymer
        while pointer is not None and pointer.next is not None:
            left = pointer.letter
            right = pointer.next.letter
            rule = rules.get(left + right, None)
            if rule is not None:
                new_part = Part(rule)
                new_part.next = pointer.next
                pointer.next = new_part

                score = scores.get(new_part.letter, 0)
                scores[new_part.letter] = score + 1

                pointer = new_part

            pointer = pointer.next

    min = None
    min_val = 9999
    max = None
    max_val = 0
    for score in scores:
        val = scores[score]
        if val < min_val:
            min = score
            min_val = val
        elif val > max_val:
            max = score
            max_val = val

    return str(max_val - min_val)


@aoc.timer
def run_challenge_two(challenge_input) -> str:
    polymer, rules = challenge_input

    groups = {}

    scores = {}
    pointer = polymer
    while pointer.next is not None:
        score = scores.get(pointer.letter, 0)
        scores[pointer.letter] = score + 1

        key = pointer.letter + pointer.next.letter
        count = groups.get(key, 0)
        groups[key] = count + 1

        pointer = pointer.next
    score = scores.get(pointer.letter, 0)
    scores[pointer.letter] = score + 1

    for i in range(40):
        new_groups = {}
        for group_key in groups:
            group_count = groups[group_key]
            if group_count == 0:
                continue
            left, right = group_key[0], group_key[1]
            new_letter = rules[group_key]
            
            key = left + new_letter
            count = new_groups.get(key, 0)
            new_groups[key] = count + group_count

            key = new_letter + right
            count = new_groups.get(key, 0)
            new_groups[key] = count + group_count

            score = scores.get(new_letter, 0)
            scores[new_letter] = score + group_count

        groups = new_groups
        

    min_val = 999999999999
    max_val = 0
    for score in scores:
        val = scores[score]
        if val < min_val:
            min_val = val
        elif val > max_val:
            max_val = val

    return str(max_val - min_val)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
import statistics

from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    # return 20, 30, -10, -5
    return 207, 263, -115, -63


def is_in_bounds(p, bounds):
    return p[0] >= bounds['x'][0] and p[0] <= bounds['x'][1] and \
        p[1] >= bounds['y'][0] and p[1] <= bounds['y'][1]


def simulate(velocity, steps, bounds):
    y_min = bounds['y'][0]
    
    p = [0, 0]
    highest_y = 0
    for i in range(steps):
        p[0] = p[0] + velocity[0]
        p[1] = p[1] + velocity[1]

        if velocity[1] > 0:
            highest_y = p[1]

        if velocity[0] > 0:
            velocity[0] = velocity[0] - 1
        velocity[1] = velocity[1] - 1
        if is_in_bounds(p, bounds):
            return True, highest_y
        
    return False, -9999


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    x_min, x_max, y_min, y_max = challenge_input
    bounds = {
        'x': [x_min, x_max],
        'y': [y_min, y_max],
    }

    dx = 0
    expected_num_steps = 1
    while True:
        dx = dx + expected_num_steps
        if dx > x_max:
            dx = dx - expected_num_steps
            expected_num_steps = expected_num_steps - 1
            break
        expected_num_steps = expected_num_steps + 1

    result = 0
    for y in range(y_min, y_max + 1, 1):
        is_in_bounds, highest_y = simulate([expected_num_steps, -y], 1000, bounds)
        if is_in_bounds:
            result = highest_y
            break

    return str(result)



@aoc.timer
def run_challenge_two(challenge_input) -> str:
    x_min, x_max, y_min, y_max = challenge_input
    bounds = {
        'x': [x_min, x_max],
        'y': [y_min, y_max],
    }

    dx = 0
    expected_num_steps = 1
    while True:
        dx = dx + expected_num_steps
        if dx > x_max:
            dx = dx - expected_num_steps
            expected_num_steps = expected_num_steps - 1
            break
        expected_num_steps = expected_num_steps + 1

    highest_y_velocity = 0
    for y in range(y_min, y_max + 1, 1):
        is_in_bounds, highest_y = simulate([expected_num_steps, -y], 1000, bounds)
        if is_in_bounds:
            highest_y_velocity = -y
            break

    valid_velocities = []
    for x in range(x_max + 1):
        for y in range(y_min, highest_y_velocity + 1, 1):
            is_in_bounds, _ = simulate([x, y], 1000, bounds)
            if is_in_bounds:
                valid_velocities.append([x, y])
                print('{},{}'.format(x, y))


    return str(len(valid_velocities))



if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
import statistics

from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    result = []
    for line in file.readlines():
        result.append([int(c) for c in line.rstrip('\n')])
    return result


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    costs = {}
    costs[(0, 0)] = 0

    len_y = len(challenge_input)
    len_x = len(challenge_input[0])

    while True:
        changed = False
        for k, v in list(costs.items()):
            x, y = k
            for xx, yy in [(x,y-1),(x+1,y),(x,y+1),(x-1,y)]: # neighbours
                if (xx < 0 or xx >= len_x):
                    continue
                if (yy < 0 or yy >= len_y):
                    continue
                new_risk = v + challenge_input[yy][xx]
                if not (xx,yy) in costs:
                    costs[(xx,yy)] = new_risk
                    changed = True
                elif costs[(xx,yy)] > new_risk:
                    costs[(xx,yy)] = new_risk
                    changed = True
        if not changed:
            break
    return str(costs[(len_x - 1, len_y - 1)])


@aoc.timer
def run_challenge_two(challenge_input) -> str:
    len_y = len(challenge_input)
    len_x = len(challenge_input[0])

    grid = []
    for dy in range(5):
        for y in range(len_y):
            line = []
            for dx in range(5):
                for x in range(len_x):
                    new_value = (challenge_input[y][x] + dx + dy - 1) % 9 + 1
                    line.append(new_value)
            grid.append(line)

    return run_challenge_one(grid)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
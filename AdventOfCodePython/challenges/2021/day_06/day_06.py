import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

class Fish():
    def __init__(self, num) -> None:
        self.num = num
        self.is_new = True

    def try_create_fish(self):
        self.num = self.num - 1
        if self.num < 0:
            self.num = 6
            self.is_new = False
            return Fish(8)
        return None

    def __repr__(self) -> str:
        return 'Fish: {}-{}'.format('x' if self.is_new else ' ', self.num)

def parse_input(file) -> List[Fish]:
    return [Fish(int(num)) for num in file.read().rstrip('\n').split(',')]


@aoc.timer
def run_challenge_one(challenge_input: List[Fish]) -> str:
    fishes = challenge_input
    iteration = 0
    while iteration < 80:  
        new_fishes = []
        for fish in fishes:
            new_fish = fish.try_create_fish()
            if new_fish is not None:
                new_fishes.append(new_fish)
        for fish in new_fishes:
            fishes.append(fish)
        iteration = iteration + 1
        print(iteration)
    return str(len(fishes))


@aoc.timer
def run_challenge_two(challenge_input: List[Fish]) -> str:
    fishes = {}
    for i in range(9):
        fishes[i] = 0
    for fish in challenge_input:
        fishes[fish.num] = fishes[fish.num] + 1

    iteration = 0
    while iteration < 256:  
        new_fishes = fishes[0]
        for i in range(1, 9):
            fishes[i - 1] = fishes[i]
        fishes[6] = fishes[6] + new_fishes
        fishes[8] = new_fishes
        iteration = iteration + 1
        print(iteration)
    count = 0
    for f in fishes:
        count = count + fishes[f]
    return str(count)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
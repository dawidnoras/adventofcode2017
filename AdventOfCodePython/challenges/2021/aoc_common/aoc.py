import os
import time

RUN_CHALLENGE_ONE = 'one'
RUN_CHALLENGE_TWO = 'two'
RUN_CHALLENGE_BOTH = 'both'

_RUN = 'run'
_CHALLENGE_ONE = 'challenge_one'
_CHALLENGE_TWO = 'challenge_two'


def _should_run(data, challenge_number):
    return data == RUN_CHALLENGE_BOTH or \
           data == RUN_CHALLENGE_ONE and challenge_number == 1 or \
           data == RUN_CHALLENGE_TWO and challenge_number == 2


def create_run_data(run_challenge_one, run_challenge_two, run):
    return {
        _CHALLENGE_ONE: run_challenge_one,
        _CHALLENGE_TWO: run_challenge_two,
        _RUN: run
    }           
           
           
def start(path, parse_input, run_data):
    os.chdir(path)

    with open('in.txt', "r") as in_file:
        challenge_input = parse_input(in_file)
        
    if _should_run(run_data[_RUN], 1):
        result_one = run_data[_CHALLENGE_ONE](challenge_input)
        with open('out_1.txt', "w") as out_file:
            out_file.write(result_one)
        print('#########-result_one-#########')
        print(result_one)
        print('##############################')       
        
    if _should_run(run_data[_RUN], 2):
        result_two = run_data[_CHALLENGE_TWO](challenge_input)
        with open('out_2.txt', "w") as out_file:
            out_file.write(result_two)
        print('#########-result_two-#########')
        print(result_two)
        print('##############################')    
        
        
def timer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        print(f"\nTime required: {(time.time() - start_time)*1000:.2f} ms\n")
        return result
    return wrapper
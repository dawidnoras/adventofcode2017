import os
import sys
import re
from collections import deque
import statistics
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


MAKE_UNIQUE_KEY = lambda x, y: '{},{}'.format(x, y)


def parse_input(file):
    dots = {}
    folds = []
    pattern_num = re.compile('^(\\d+),(\\d+)')
    pattern_fold = re.compile('^fold along (\\w)=(\\d+)')
    for line in file.readlines():
        line = line.rstrip('\n')
        match = pattern_num.match(line)
        if match is not None:
            (sx, sy) = match.groups()
            x, y = int(sx), int(sy)
            unique_key = MAKE_UNIQUE_KEY(x, y)
            dots[unique_key] = [x, y]
        else:
            match = pattern_fold.match(line)
            if match is not None:
                (axis, num) = match.groups()
                folds.append({
                    'axis': axis,
                    'num': int(num)
                })

    return dots, folds


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    dots, folds = challenge_input
    
    new_dots = {}
    dots_folded = []

    fold = folds[0]
    fold_num = fold['num']
    if fold['axis'] == 'x':
        for key in dots:
            dot = dots[key]
            if dot[0] < fold_num:
                continue
            new_x = fold_num - (dot[0] - fold_num)
            unique_key = MAKE_UNIQUE_KEY(new_x, dot[1])
            new_dots[unique_key] = [new_x, dot[1]]
            dots_folded.append(key)
    else:
        for key in dots:
            dot = dots[key]
            if dot[1] < fold_num:
                continue
            new_y = fold_num - (dot[1] - fold_num)
            unique_key = MAKE_UNIQUE_KEY(dot[0], new_y)
            new_dots[unique_key] = [dot[0], new_y]
            dots_folded.append(key)

    for key in dots_folded:
        del dots[key]
    for key in new_dots:
        dots[key] = new_dots[key]

    return str(len(dots))


@aoc.timer
def run_challenge_two(challenge_input) -> str:
    dots, folds = challenge_input
    
    for fold in folds:
        new_dots = {}
        dots_folded = []

        fold_num = fold['num']
        if fold['axis'] == 'x':
            for key in dots:
                dot = dots[key]
                if dot[0] < fold_num:
                    continue
                new_x = fold_num - (dot[0] - fold_num)
                unique_key = MAKE_UNIQUE_KEY(new_x, dot[1])
                new_dots[unique_key] = [new_x, dot[1]]
                dots_folded.append(key)
        else:
            for key in dots:
                dot = dots[key]
                if dot[1] < fold_num:
                    continue
                new_y = fold_num - (dot[1] - fold_num)
                unique_key = MAKE_UNIQUE_KEY(dot[0], new_y)
                new_dots[unique_key] = [dot[0], new_y]
                dots_folded.append(key)

        for key in dots_folded:
            del dots[key]
        for key in new_dots:
            dots[key] = new_dots[key]


    for y in range(10):
        s = ''
        for x in range(200):
            key = MAKE_UNIQUE_KEY(x, y)
            ds = '#' if dots.get(key, None) is not None else '.'
            s = s + ds
        print(s)

    return str(0)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
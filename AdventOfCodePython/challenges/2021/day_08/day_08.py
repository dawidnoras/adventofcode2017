import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

#  0 0 0
# 1     2
# 1     2
#  3 3 3
# 4     5
# 4     5
#  6 6 6


ALL_SIGNALS = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

class Display():
    def __init__(self, signals, digits) -> None:
        self.signals = signals
        self.digits = digits 

    def decode(self, digit):
        for i in range(10):
            signal = self.known_digits[i]
            if len(signal) != len(digit):
                continue
            counter = 0
            for c in digit:
                if c not in signal:
                    break
                counter = counter + 1
            if counter == len(digit):
                return i
        return None

    def prepare_for_challenge_two(self):
        self.known_signals = []
        for i in range(7):
            self.known_signals.append([])
            for signal in ALL_SIGNALS:
                self.known_signals[i].append(signal)

        self.known_digits = []
        for i in range(10):
            self.known_digits.append(None)
        
        self.known_digits[1] = next(filter(lambda x: len(x) == 2, self.signals))
        self.known_digits[7] = next(filter(lambda x: len(x) == 3, self.signals))
        self.known_digits[4] = next(filter(lambda x: len(x) == 4, self.signals))
        self.known_digits[8] = next(filter(lambda x: len(x) == 7, self.signals))

        # digit 1
        self.known_signals[2].clear()
        self.known_signals[5].clear()
        for c in self.known_digits[1]:
            self.known_signals[2].append(c)
            self.known_signals[5].append(c)

        # digit 7
        for c in self.known_digits[7]:
            if c not in self.known_signals[2]:
                self.known_signals[0] = [c]
                break

        # digit 4
        self.known_signals[1].clear()
        self.known_signals[3].clear()       
        for c in self.known_digits[4]:
            if c not in self.known_signals[2]:
                self.known_signals[1].append(c)
                self.known_signals[3].append(c)

        # digit 8       
        self.known_signals[4].clear()
        self.known_signals[6].clear()       
        for c in self.known_digits[8]:
            if c not in self.known_digits[4] and c not in self.known_signals[0]:
                self.known_signals[4].append(c)
                self.known_signals[6].append(c)

        # digit 3 will have both segments 2 and 5. (segments of digit 1) - len=5
        for digit_len_5 in filter(lambda x: len(x) == 5, self.signals):
            count = 0
            for c in self.known_digits[1]:
                if c in digit_len_5:
                    count = count + 1
            if count == 2:      
                self.known_digits[3] = digit_len_5
                # for c in digit_len_5:
                #     self.known_digits[3].append(c)
                break

        # check 4 and 6 signals against digit 3
        if self.known_signals[4][0] in self.known_digits[3]:
            self.known_signals[6] = [self.known_signals[4][0]]
            self.known_signals[4] = [self.known_signals[4][1]]
        else:
            self.known_signals[6] = [self.known_signals[4][1]]
            self.known_signals[4] = [self.known_signals[4][0]]
        # check 1 and 3 signals against digit 3
        if self.known_signals[1][0] in self.known_digits[3]:
            self.known_signals[3] = [self.known_signals[1][0]]
            self.known_signals[1] = [self.known_signals[1][1]]
        else:
            self.known_signals[3] = [self.known_signals[1][1]]
            self.known_signals[1] = [self.known_signals[1][0]]

        occurance_counter = {}
        for signal in self.signals:
            for c in signal:
                if c not in occurance_counter:
                    occurance_counter[c] = 1
                else:
                    occurance_counter[c] = occurance_counter[c] + 1
        # signal with 6 occurances is segment 1
        self.known_signals[1] = [next(filter(lambda x: occurance_counter[x] == 6, occurance_counter))]
       
        # signal with 4 occurances is segment 4
        self.known_signals[4] = [next(filter(lambda x: occurance_counter[x] == 4, occurance_counter))]
        
        # signal with 9 occurances is segment 5
        self.known_signals[5] = [next(filter(lambda x: occurance_counter[x] == 9, occurance_counter))]
        for signal in self.known_signals[2]:
            if signal not in self.known_signals[5]:
                self.known_signals[2] = [signal]
                break

        # 0 2 5 6 9
        for digit_len_6 in filter(lambda x: len(x) == 6, self.signals):
            if self.known_signals[2][0] in digit_len_6:
                if self.known_signals[3][0] in digit_len_6:
                    self.known_digits[9] = digit_len_6
                else:
                    self.known_digits[0] = digit_len_6
            else:
                self.known_digits[6] = digit_len_6

        for digit_len_5 in filter(lambda x: len(x) == 5, self.signals):
            if self.known_signals[2][0] not in digit_len_5:
                self.known_digits[5] = digit_len_5
            else:
                if self.known_signals[4][0] in digit_len_5:
                    self.known_digits[2] = digit_len_5

        # # digit 9 will have both segments 2 and 5. (segments of digit 1) - len=6
        # for digit_len_6 in filter(lambda x: len(x) == 6, self.signals):
        #     count = 0
        #     for c in self.known_digits[1]:
        #         if c in digit_len_6:
        #             count = count + 1
        #     if count == 2:      
        #         self.known_digits[9] = digit_len_6
        #         # for c in digit_len_6:
        #         #     self.known_digits[9].append(c)
        #         break
        # # the other one with len=6 will be 6
        # for another_digit_len_6 in filter(lambda x: len(x) == 6, self.signals):
        #     for c in another_digit_len_6:
        #         if c not in digit_len_6:
        #             self.known_digits[6] = another_digit_len_6
        #             # for c in another_digit_len_6:
        #             #     self.known_digits[6].append(c)
        #             break
        # # find segment 2 from digit 6
        # for c in ALL_SIGNALS:
        #     if c not in self.known_digits[6]:
        #         self.known_signals[2] = [c]
        #         break
        # 
        pass
        # digit 5 will not have segment 2. (one of segments of digit 1) - len=5
        # digit 2 will not have segment 2. (one of segments of digit 1) - len=5
        

        # digit 6 will not have segment 2. (one of segments of digit 1) - len=6

        # for self


def parse_input(file) -> List[Display]:
    result = []
    pattern = re.compile('^(\\w+) (\\w+) (\\w+) (\\w+) (\\w+) (\\w+) (\\w+) (\\w+) (\\w+) (\\w+) \\| (\\w+) (\\w+) (\\w+) (\\w+)')
    for line in file.readlines():
        line = line.rstrip('\n')
        match = pattern.match(line)
        if match is not None:
            (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, d0, d1, d2, d3) = match.groups()
            result.append(Display(
                [s0, s1, s2, s3, s4, s5, s6, s7, s8, s9],
                [d0, d1, d2, d3]))
    return result


@aoc.timer
def run_challenge_one(challenge_input: List[Display]) -> str:
    digit_lengths = [2, 3, 4, 7]
    sum = 0
    for display in challenge_input:
        for digit in display.digits:
            if len(digit) in digit_lengths:
                sum = sum + 1
    return str(sum)


@aoc.timer
def run_challenge_two(challenge_input: List[Display]) -> str:
    sum = 0
    for display in challenge_input:
        display.prepare_for_challenge_two()
        value = 0
        for digit in display.digits:
            value = value * 10
            value = value + display.decode(digit)
        sum = sum + value

    return str(sum)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
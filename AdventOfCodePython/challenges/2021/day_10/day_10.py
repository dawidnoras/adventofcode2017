import os
import sys
import re
from collections import deque
import statistics
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

def parse_input(file) -> List[str]:
    result = []
    for line in file.readlines():
        line = line.rstrip('\n')
        result.append(line)
    return result


OPEN_BRACKETS = [
    '(',
    '[',
    '{',
    '<',
]

CLOSE_BRACKETS_SCORES = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

CLOSE_BRACKETS_MAP = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

CLOSE_BRACKETS_COMPLETION_SCORES = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

@aoc.timer
def run_challenge_one(challenge_input: List[str]) -> str:
    stack = deque()
    corrupted_symbols = []
    for line in challenge_input:
        for c in line:
            if c in OPEN_BRACKETS:
                stack.append(c)
            else:
                last_open = stack.pop()
                if CLOSE_BRACKETS_MAP[last_open] != c:
                    corrupted_symbols.append(c)
                    break
    s = 0
    for c in corrupted_symbols:
        s = s + CLOSE_BRACKETS_SCORES[c]
    return str(s)


@aoc.timer
def run_challenge_two(challenge_input: List[str]) -> str:
    completion_strings = []
    for line in challenge_input:
        stack = deque()
        is_corrupted = False
        for i in range(len(line)):
            c = line[i]
            if c in OPEN_BRACKETS:
                stack.append(c)
            else:
                last_open = stack.pop()
                if CLOSE_BRACKETS_MAP[last_open] != c:
                    is_corrupted = True
                    continue
        if is_corrupted:
            continue
        if len(line) == i + 1:
            stack.reverse()
            completion_strings.append(list(stack))
    scores = []
    for completion in completion_strings:
        score = 0
        for c in completion:
            score = score * 5 + CLOSE_BRACKETS_COMPLETION_SCORES[CLOSE_BRACKETS_MAP[c]]
        scores.append(score)
    s = statistics.median(scores)
    return str(s)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
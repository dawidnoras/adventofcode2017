import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file) -> List[int]:
    return [line.rstrip('\n') for line in file.readlines()]


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    gamma_rate = 0
    epsilon_rate = 0
    for i in range(len(challenge_input[0])):
        more_one = 0
        for line in challenge_input:
            if line[i] == '0':
                more_one = more_one - 1
            else:
                more_one = more_one + 1
        
        gamma_rate = gamma_rate << 1
        epsilon_rate = epsilon_rate << 1
        if more_one > 0:
            gamma_rate = gamma_rate + 1
        else:
            epsilon_rate = epsilon_rate + 1

    return str(gamma_rate * epsilon_rate)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    potential_oxygen_ratings = list(range(len(challenge_input)))
    potential_co2_ratings = list(range(len(challenge_input)))
    for i in range(len(challenge_input[0])):
        # oxygen
        if len(potential_oxygen_ratings) > 1:
            ones_count = 0
            for index in potential_oxygen_ratings:
                line = challenge_input[index]
                if line[i] == '0':
                    ones_count = ones_count - 1
                else:
                    ones_count = ones_count + 1

            to_delete = []
            good_oxygen = '1' if ones_count > 0 or ones_count == 0 else '0'
            for index in potential_oxygen_ratings:
                potential_oxygen_rating = challenge_input[index]
                if potential_oxygen_rating[i] != good_oxygen:
                    to_delete.append(index)
            for index in to_delete:
                potential_oxygen_ratings.remove(index)

        # co2
        if len(potential_co2_ratings) > 1:
            ones_count = 0
            for index in potential_co2_ratings:
                line = challenge_input[index]
                if line[i] == '0':
                    ones_count = ones_count - 1
                else:
                    ones_count = ones_count + 1

            to_delete = []
            good_co2 = '0' if ones_count > 0 or ones_count == 0 else '1'
            for index in potential_co2_ratings:
                potential_co2_rating = challenge_input[index]
                if potential_co2_rating[i] != good_co2:
                    to_delete.append(index)
            for index in to_delete:
                potential_co2_ratings.remove(index)

    oxygen = challenge_input[potential_oxygen_ratings[0]]
    co2 = challenge_input[potential_co2_ratings[0]]
    result = int(oxygen, 2) * int(co2, 2)
    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
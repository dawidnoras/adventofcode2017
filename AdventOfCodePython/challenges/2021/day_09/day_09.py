import os
import sys
import re
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

def parse_input(file) -> List[List[int]]:
    result = []
    for line in file.readlines():
        line = line.rstrip('\n')
        result.append([int(c) for c in line])
    return result


@aoc.timer
def run_challenge_one(challenge_input: List[List[int]]) -> str:
    len_x = len(challenge_input[0])
    len_y = len(challenge_input)
    
    danger_number = 0
    for y in range(len(challenge_input)):
        for x in range(len(challenge_input[0])):
            current_num = challenge_input[y][x]
            is_lowest = True
            # check adjenced
            for dy in [-1, 0, 1]:
                if not is_lowest:
                    break
                for dx in [-1, 0, 1]:
                    if dy == dx == 0:
                        continue
                    nx = x + dx
                    ny = y + dy
                    if nx < 0 or nx >= len_x:
                        continue
                    if ny < 0 or ny >= len_y:
                        continue

                    if challenge_input[ny][nx] <= current_num:
                        is_lowest = False
                    
                    if not is_lowest:
                        break
            if is_lowest:
                danger_number = danger_number + current_num + 1
    return str(danger_number)


CHECK_LIST = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
]
MAKE_UNIQUE_KEY = lambda x, y: '{},{}'.format(x, y)
MAKE_UNIQUE_KEY_POINT = lambda point: '{},{}'.format(point['x'], point['y'])
LEN_X = 0
LEN_Y = 0

def check_points(input, points_to_check: List, already_checked_points: Dict) -> int:
    new_points = []
    for point in points_to_check:
        if MAKE_UNIQUE_KEY_POINT(point) in already_checked_points:
            continue
        for check in CHECK_LIST:
            nx = point['x'] + check[0]
            ny = point['y'] + check[1]
            if nx < 0 or nx >= LEN_X:
                continue
            if ny < 0 or ny >= LEN_Y:
                continue
            if input[ny][nx] == 9:
                continue
            new_points.append({
                'x': nx,
                'y': ny
            })
        already_checked_points[MAKE_UNIQUE_KEY_POINT(point)] = True

    if len(new_points) == 0:
        return 

    check_points(input, new_points, already_checked_points)



@aoc.timer
def run_challenge_two(challenge_input: List[str]) -> str:
    global LEN_X
    global LEN_Y
    LEN_X = len(challenge_input[0])
    LEN_Y = len(challenge_input)
    
    basins = []
    for y in range(len(challenge_input)):
        for x in range(len(challenge_input[0])):
            current_num = challenge_input[y][x]
            is_lowest = True
            # check adjenced
            for dy in [-1, 0, 1]:
                if not is_lowest:
                    break
                for dx in [-1, 0, 1]:
                    if dy == dx == 0:
                        continue
                    nx = x + dx
                    ny = y + dy
                    if nx < 0 or nx >= LEN_X:
                        continue
                    if ny < 0 or ny >= LEN_Y:
                        continue

                    if challenge_input[ny][nx] <= current_num:
                        is_lowest = False
                    
                    if not is_lowest:
                        break
            if is_lowest:
                basins.append({
                    'x': x,
                    'y': y
                })

    basin_sizes = []
    for basin_starter in basins:
        already_checked_points = {}
        points_to_check = [basin_starter]
        check_points(challenge_input, points_to_check, already_checked_points)
        basin_sizes.append(len(already_checked_points))

    basin_sizes.sort(reverse=True)
    result = basin_sizes[0] * basin_sizes[1] * basin_sizes[2]

    return str(result)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
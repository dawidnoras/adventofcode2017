import os
import sys
import re
from typing import List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc

class Board():
    def __init__(self, rows):
        self.rows = [[{'num': num, 'checked': False} for num in row] for row in rows]
        self.is_completed = False

    def check_num(self, num):
        for row in self.rows:
            for cell in row:
                if cell['num'] == num and not cell['checked']:
                    cell['checked'] = True
                    return self.check_board()
        return False

    def check_board(self):
        for row in self.rows:
            if all(num['checked'] for num in row):
                self.is_completed = True
                return True
        for i in range(5):
            column = [self.rows[col_index][i] for col_index in range(5)]
            if all(num['checked'] for num in column):
                self.is_completed = True
                return True
        return False


def parse_input(file) -> List[int]:
    patern_row = re.compile('(\\d+) +(\\d+) +(\\d+) +(\\d+) +(\\d+)')
    
    line = file.readline().rstrip('\n')
    numbers = [int(i) for i in line.split(',')]

    boards = []
    line = file.readline()
    while True:
        if line == '':
            break
        if line == '\n':
            rows = []
            for i in range(5):
                line = file.readline().rstrip('\n').lstrip(' ')
                match = patern_row.match(line)
                (a0, a1, a2, a3, a4) = match.groups()
                rows.append([int(a0), int(a1), int(a2), int(a3), int(a4)])
            boards.append(Board(rows))
            line = file.readline()

    return numbers, boards


@aoc.timer
def run_challenge_one(challenge_input: List[int]) -> str:
    numbers, boards = challenge_input
    for num in numbers:
        for board in boards:
            if board.check_num(num):
                break
        if board.is_completed:
            break

    sum_numbers = 0
    for row in board.rows:
        for board_num in row:
            if not board_num['checked']:
                sum_numbers = sum_numbers + board_num['num']
    return str(sum_numbers * num)


@aoc.timer
def run_challenge_two(challenge_input: List[int]) -> str:
    numbers, boards = challenge_input
    numb_completed_boards = 0
    boards_len = len(boards)
    for num in numbers:
        for board in boards:
            if not board.is_completed:
                if board.check_num(num):
                    numb_completed_boards = numb_completed_boards + 1
                    if numb_completed_boards == boards_len:
                        break
        if numb_completed_boards == boards_len:
            break

    sum_numbers = 0
    for row in board.rows:
        for board_num in row:
            if not board_num['checked']:
                sum_numbers = sum_numbers + board_num['num']
    return str(sum_numbers * num)


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
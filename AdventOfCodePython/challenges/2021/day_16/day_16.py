import os
import sys
import re
import statistics

from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


def parse_input(file):
    hex_str = file.readline().rstrip('\n')
    n = int(hex_str, 16) 
    bStr = ''
    while n > 0:
        bStr = str(n % 2) + bStr
        n = n >> 1 
    ll = len(bStr)
    if ll % 4 > 0:
        bStr = ('0' * (4 - (ll % 4))) + bStr

    # anotherbStr = hex_to_binary(hex_str, len(hex_str) * 4)
    return bStr


def parse_next_packet(slice, slice_index):
    debug_str = ''
    lll = len(slice)
    for x in range(len(slice)):
        if x < slice_index:
            debug_str = debug_str + '-'
        else:
            debug_str = debug_str + slice[x]
    i = slice_index
    version = slice[i + 0: i + 3]
    type_id = slice[i + 3: i + 6]
    literal = ''
    child_packets = []
    if type_id == '100':
        i = slice_index + 6
        while True:
            next_four = slice[i:i + 5]
            literal = literal + next_four[1:5]
            i = i + 5
            if next_four[0] == '0':
                break

    else: #type_id == '110':
        len_type_id = slice[slice_index + 6]
        if len_type_id == '0':
            num_length = 15
            i = slice_index + 7 + num_length
            length_of_sub_packets = slice[slice_index + 7:i]
            length_of_sub_packets_int = int(length_of_sub_packets, 2)
            # read child packets
            end = i + length_of_sub_packets_int
            while i < end:
                next_index, new_packet = parse_next_packet(slice, i)
                child_packets.append(new_packet)
                i = next_index

        elif len_type_id == '1':
            num_length = 11
            i = slice_index + 7 + num_length
            number_of_child_packets = slice[slice_index + 7:i]
            number_of_child_packets_int = int(number_of_child_packets, 2)
            for x in range(number_of_child_packets_int):
                next_index, new_packet = parse_next_packet(slice, i)
                child_packets.append(new_packet)
                i = next_index
        
    # else:
    #     raise Exception('wrong type Id')

    # l = len(slice)
    # while i < l and i % 4 != 1 and slice[i] == '0':
    #     i = i + 1
    next_index = i

    return next_index, {
        'v': version,
        'type': type_id,
        'literal': -1 if len(literal) == 0 else int(literal, 2),
        'child_packets': child_packets
    }


def sum_versions(packet):
    s = int(packet['v'], 2)
    for cp in packet['child_packets']:
        s = s + sum_versions(cp)
    return s


@aoc.timer
def run_challenge_one(challenge_input) -> str:
    next_index, new_packet = parse_next_packet(challenge_input, 0)
    v = sum_versions(new_packet)
    return str(v)


def calculate(packet):
    action = int(packet['type'], 2)
    len_sub_packets = len(packet['child_packets'])
    if action == 4:
        return packet['literal']
    elif action == 0:
        # sum
        sum_result = 0
        for cp in packet['child_packets']:
            sum_result = sum_result + calculate(cp)
        return sum_result
    elif action == 1:
        # product
        product_result = 1
        for cp in packet['child_packets']:
            product_result = product_result * calculate(cp)
        return product_result
    elif action == 2:
        # minimum
        values = []
        for cp in packet['child_packets']:
            values.append(calculate(cp))
        result = min(values)
        return result
    elif action == 3:
        # maximum
        values = []
        for cp in packet['child_packets']:
            values.append(calculate(cp))
        result = max(values)
        return result
    elif action == 5:
        # greater than
        a = calculate(packet['child_packets'][0])
        b = calculate(packet['child_packets'][1])
        result = 1 if a > b else 0
        return result
    elif action == 6:
        # less than
        a = calculate(packet['child_packets'][0])
        b = calculate(packet['child_packets'][1])
        result = 1 if a < b else 0
        return result
    elif action == 7:
        # equal to
        a = calculate(packet['child_packets'][0])
        b = calculate(packet['child_packets'][1])
        result = 1 if a == b else 0
        return result
    pass


@aoc.timer
def run_challenge_two(challenge_input) -> str:
    next_index, new_packet = parse_next_packet(challenge_input, 0)
    result = calculate(new_packet)
    return str(result)



if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_TWO)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
import os
import sys
import re
from collections import deque
import statistics
from typing import Dict, List
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from aoc_common import aoc


class Node():
    def __init__(self, name) -> None:
        self.name = name
        self.is_big = name.isupper()
        self.is_start = name == 'start'
        self.is_end = name == 'end'
        self.connections = []
    
    def __repr__(self) -> str:
        return 'Node: {}'.format(self.name)


def parse_input(file) -> Dict[str, Node]:
    nodes = {}
    pattern = re.compile('^(\\w+)-(\\w+)')
    for line in file.readlines():
        line = line.rstrip('\n')
        match = pattern.match(line)
        if match is not None:
            (node_a_name, node_b_name) = match.groups()
            if node_a_name in nodes:
                node_a = nodes[node_a_name]
            else:
                node_a = Node(node_a_name)
                nodes[node_a_name] = node_a

            if node_b_name in nodes:
                node_b = nodes[node_b_name]
            else:
                node_b = Node(node_b_name)
                nodes[node_b_name] = node_b

            node_a.connections.append(node_b)
            node_b.connections.append(node_a)

    return nodes


class Path():
    def __init__(self, slice: List[Node]) -> None:
        self.path = []
        self.visited_nodes = {}
        self.small_cave = None
        for node in slice:
            self.path.append(node)
            self.visited_nodes[node.name] = node
        
    def add_node(self, node):
        self.path.append(node)
        self.visited_nodes[node.name] = node

    def __repr__(self) -> str:
        return 'Path: [{}]'.format('-'.join([node.name for node in self.path]))


@aoc.timer
def run_challenge_one(challenge_input: Dict[str, Node]) -> str:
    start_node = challenge_input['start']
    end_node = challenge_input['end']

    finished_paths = []
    paths_to_check = [Path([start_node])]

    while len(paths_to_check) > 0:
        new_paths = []
        for path_to_check in paths_to_check:
            last_node = path_to_check.path[-1]

            available_connections = []
            for connection in last_node.connections:
                if not connection.is_big and connection.name in path_to_check.visited_nodes:
                    # already visited this big node
                    continue
                available_connections.append(connection)

            available_connections_len = len(available_connections)
            if available_connections_len == 0:
                # discard the path. dead end
                pass
            elif available_connections_len == 1:
                # just add the node to the path
                path_to_check.add_node(available_connections[0])
                if available_connections[0] == end_node:
                    # end the path. reached the end
                    finished_paths.append(path_to_check)
                else:
                    # readd the path
                    new_paths.append(path_to_check)
            else:
                # make new paths and discard old one
                for connection in available_connections:
                    new_path = Path(path_to_check.path)
                    new_path.add_node(connection)
                    if connection == end_node:
                        # end the path. reached the end
                        finished_paths.append(new_path)
                    else:
                        # readd the path
                        new_paths.append(new_path)

        paths_to_check = new_paths

    return str(len(finished_paths))


@aoc.timer
def run_challenge_two(challenge_input: Dict[str, Node]) -> str:
    start_node = challenge_input['start']
    end_node = challenge_input['end']

    finished_paths = []
    paths_to_check = [Path([start_node])]

    while len(paths_to_check) > 0:
        new_paths = []
        for path_to_check in paths_to_check:
            last_node = path_to_check.path[-1]

            available_connections = []
            for connection in last_node.connections:
                if not connection.is_big and connection.name in path_to_check.visited_nodes:
                    # already visited this small node
                    if path_to_check.small_cave is not None:
                        continue
                        # allow one small cave to go in twice
                if connection == start_node:
                    continue
                available_connections.append(connection)

            available_connections_len = len(available_connections)
            if available_connections_len == 0:
                # discard the path. dead end
                pass
            elif available_connections_len == 1:
                # just add the node to the path
                path_to_check.add_node(available_connections[0])
                if available_connections[0] == end_node:
                    # end the path. reached the end
                    finished_paths.append(path_to_check)
                else:
                    # readd the path
                    if not available_connections[0].is_big and available_connections[0].name in path_to_check.visited_nodes:
                        path_to_check.small_cave = available_connections[0]

                    new_paths.append(path_to_check)
            else:
                # make new paths and discard old one
                for connection in available_connections:
                    new_path = Path(path_to_check.path)
                    new_path.small_cave = path_to_check.small_cave
                    new_path.add_node(connection)
                    if connection == end_node:
                        # end the path. reached the end
                        finished_paths.append(new_path)
                    else:
                        # readd the path
                        if not connection.is_big and connection.name in path_to_check.visited_nodes:
                            new_path.small_cave = connection

                        new_paths.append(new_path)

        paths_to_check = new_paths

    return str(len(finished_paths))


if __name__ == '__main__':
    run_data = aoc.create_run_data(run_challenge_one,
                                   run_challenge_two,
                                   aoc.RUN_CHALLENGE_BOTH)
    aoc.start(os.path.dirname(__file__),
              parse_input,
              run_data)
    
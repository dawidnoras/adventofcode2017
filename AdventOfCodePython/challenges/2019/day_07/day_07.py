import itertools
import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Program:
    def __init__(self, int_code, input_data):
        self.input_data = input_data
        self.input_data_index = 0
        self.output_data = []
        self.int_code = int_code
        self.halted = False
        self.paused = False
        self.instruction_pointer = 0
        self.jump_performed = False

        self.instruction_definitions = {
            1: {'name': 'add',
                'action': lambda params: self.add(params[0], params[1], params[2]),
                'write_to_parameter_index': 2,
                'number_of_parameters': 3},
            2: {'name': 'multiply',
                'action': lambda params: self.multiply(params[0], params[1], params[2]),
                'write_to_parameter_index': 2,
                'number_of_parameters': 3},
            3: {'name': 'store_input',
                'action': lambda params: self.store_input(params[0]),
                'write_to_parameter_index': 0,
                'number_of_parameters': 1},
            4: {'name': 'write_output',
                'action': lambda params: self.write_output(params[0]),
                'write_to_parameter_index': None,
                'number_of_parameters': 1},
            5: {'name': 'jump_if_true',
                'action': lambda params: self.jump_if(params[0] != 0, params[1]),
                'write_to_parameter_index': None,
                'number_of_parameters': 2},
            6: {'name': 'jump_if_false',
                'action': lambda params: self.jump_if(params[0] == 0, params[1]),
                'write_to_parameter_index': None,
                'number_of_parameters': 2},
            7: {'name': 'less_than',
                'action': lambda params: self.compare(params[0] < params[1], params[2]),
                'write_to_parameter_index': 2,
                'number_of_parameters': 3},
            8: {'name': 'equals',
                'action': lambda params: self.compare(params[0] == params[1], params[2]),
                'write_to_parameter_index': 2,
                'number_of_parameters': 3},

            99: {'name': 'halt',
                 'action': lambda params: self.halt(),
                 'write_to_parameter_index': None,
                 'number_of_parameters': 0},
        }

        self.parameter_modes = {
            0: {'name': 'position', 'transform': lambda a, code=self.int_code: code[a]},
            1: {'name': 'immediate', 'transform': lambda a: a},
        }

    def add(self, a, b, result_index):
        self.int_code[result_index] = a + b

    def multiply(self, a, b, result_index):
        self.int_code[result_index] = a * b

    def store_input(self, store_index):
        self.int_code[store_index] = self.input_data[self.input_data_index]
        if self.input_data_index < len(self.input_data) - 1:
            self.input_data_index += 1

    def write_output(self, output):
        self.output_data.append(output)
        self.paused = True

    def jump_if(self, condition, value):
        if condition:
            self.instruction_pointer = value
            self.jump_performed = True

    def compare(self, condition, address):
        if condition:
            self.int_code[address] = 1
        else:
            self.int_code[address] = 0

    def halt(self):
        self.halted = True

    def run(self):
        while not self.halted and not self.paused:
            int_instruction = self.int_code[self.instruction_pointer]
            opt_code = int_instruction % 100
            instruction = self.instruction_definitions[opt_code]
            parameters = []
            n = int_instruction // 100
            for i in range(instruction['number_of_parameters']):
                parameter_mode_number = n % 10
                parameter_mode = self.parameter_modes[parameter_mode_number]
                parameter = self.int_code[self.instruction_pointer + i + 1]
                #  write to parameter will always be in position mode, it needs to be passed as position
                if instruction['write_to_parameter_index'] != i:
                    transformed_parameter = parameter_mode['transform'](parameter)
                else:
                    transformed_parameter = parameter
                parameters.append(transformed_parameter)
                n = n // 10
            instruction['action'](parameters)

            if not self.jump_performed:
                self.instruction_pointer += instruction['number_of_parameters'] + 1
            else:
                self.jump_performed = False

    def continue_run(self):
        self.paused = False
        self.run()


def parse_input(file):
    return [int(number) for number in file.read().rstrip().split(',')]


def run_amplifier(int_code, inputs):
    program = Program(int_code, inputs)
    program.run()
    return program.output_data[-1]


def try_run_amplifiers(int_code, phase_settings_sequence):
    output = 0
    for i in range(5):
        output = run_amplifier(int_code[:], [phase_settings_sequence[i], output])
    return output


def run_challenge_one(challenge_input):
    max_output = 0
    permutations = itertools.permutations([0, 1, 2, 3, 4], 5)
    for permutation in permutations:
        output = try_run_amplifiers(challenge_input, permutation)
        if output > max_output:
            max_output = output
    return str(max_output)


class Amplifier:
    def __init__(self, index, int_code, phase_settings_number):
        self.index = index
        self.program = Program(int_code, [phase_settings_number, -1])
        self.phase_settings_number = phase_settings_number

    def run(self, in_data):
        self.program.input_data[1] = in_data
        self.program.continue_run()
        return self.program.output_data[-1]


def run_looped_amplifiers(int_code, phase_settings_sequence):
    amplifiers = []
    for i in range(5):
        amplifiers.append(Amplifier(i, int_code[:], phase_settings_sequence[i]))
    output = 0
    while not amplifiers[len(amplifiers) - 1].program.halted:
        for amplifier in amplifiers:
            output = amplifier.run(output)
    return output


def run_challenge_two(challenge_input):
    max_output = 0
    permutations = itertools.permutations([5, 6, 7, 8, 9], 5)
    for permutation in permutations:
        output = run_looped_amplifiers(challenge_input, permutation)
        if output > max_output:
            max_output = output
    return str(max_output)

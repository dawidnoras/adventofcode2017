import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Asteroid:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.visible_asteroids = []

    def __repr__(self):
        return '{}x{}'.format(self.x, self.y)

    def calculate_visible_asteroids(self, asteroids):
        sorted_asteroids = sorted(asteroids[:], key=lambda a: abs(self.x - a.x) + abs(self.y - a.y))
        for asteroid in sorted_asteroids:
            if asteroid == self:
                continue
            if self.check_line_of_sight(asteroid):
                self.visible_asteroids.append(asteroid)
        return len(self.visible_asteroids)

    def check_line_of_sight(self, asteroid):
        diff_position = self.x - asteroid.x, self.y - asteroid.y
        diff_position_length = abs(diff_position[0] + diff_position[1])
        for visible_asteroid in self.visible_asteroids:
            visible_asteroid_diff_position = self.x - visible_asteroid.x, self.y - visible_asteroid.y
            visible_asteroid_diff_position_length = abs(visible_asteroid_diff_position[0] + visible_asteroid_diff_position[1])
            check_length = visible_asteroid_diff_position_length
            hide_position = visible_asteroid_diff_position[0] * 2, visible_asteroid_diff_position[1] * 2
            while check_length < diff_position_length:
                if diff_position[0] == hide_position[0] and diff_position[1] == hide_position[1]:
                    return False
                check_length += diff_position_length

            if diff_position[0] == 0 and visible_asteroid_diff_position[0] == 0:
                if diff_position[1] * visible_asteroid_diff_position[1] >= 0:
                    return False
                else:
                    continue

            if diff_position[1] == 0 and visible_asteroid_diff_position[1] == 0:
                if diff_position[0] * visible_asteroid_diff_position[0] >= 0:
                    return False
                else:
                    continue

            if diff_position[1] != 0 and visible_asteroid_diff_position[1] != 0:
                factor = diff_position[0] / diff_position[1]
                visible_factor = visible_asteroid_diff_position[0] / visible_asteroid_diff_position[1]
                if factor == visible_factor:
                    if diff_position[1] * visible_asteroid_diff_position[1] >= 0:
                        return False
                    else:
                        continue

            if diff_position[0] != 0 and visible_asteroid_diff_position[0] != 0:
                factor = diff_position[1] / diff_position[0]
                visible_factor = visible_asteroid_diff_position[1] / visible_asteroid_diff_position[0]
                if factor == visible_factor:
                    if diff_position[0] * visible_asteroid_diff_position[0] >= 0:
                        return False
                    else:
                        continue
        return True


def parse_input(file):
    tab = []
    for line in file.readlines():
        line = line.rstrip('\n')
        line = [c for c in line]
        tab.append(line)
    asteroids_map = []
    asteroids = []
    for y in range(len(tab)):
        asteroid_row = []
        for x in range(len(tab[0])):
            if tab[y][x] == '#':
                asteroid = Asteroid(x, y)
                asteroid_row.append(asteroid)
                asteroids.append(asteroid)
            else:
                asteroid_row.append(None)
        asteroids_map.append(asteroid_row)
    return asteroids_map, asteroids


def run_challenge_one(challenge_input):
    asteroids_map, asteroids = challenge_input
    for asteroid in asteroids:
        asteroid.calculate_visible_asteroids(asteroids)
    max_visible_asteroids = max(asteroids, key=lambda a: len(a.visible_asteroids))
    return str(len(max_visible_asteroids.visible_asteroids))


def run_challenge_two(challenge_input):
    asteroids_map, asteroids = challenge_input
    for asteroid in asteroids:
        asteroid.calculate_visible_asteroids(asteroids)
    max_visible_asteroids = max(asteroids, key=lambda a: len(a.visible_asteroids))


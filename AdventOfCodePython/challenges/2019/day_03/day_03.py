import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Vector:
    def __init__(self, direction, length):
        self.start_point = [0, 0]
        self.direction = direction
        self.length = length

    def check_for_intersection(self, other_vector):
        x11 = self.start_point[0]
        y11 = self.start_point[1]
        x12 = x11 + self.direction[0] * self.length
        y12 = y11 + self.direction[1] * self.length

        x21 = other_vector.start_point[0]
        y21 = other_vector.start_point[1]
        x22 = x21 + other_vector.direction[0] * other_vector.length
        y22 = y21 + other_vector.direction[1] * other_vector.length

        def is_between(value, number_one, number_two):
            return min(number_one, number_two) < value < max(number_one, number_two)

        #  self is horizontal, compare against vertical
        if self.direction[0] != 0 and other_vector.direction[0] == 0:
            if is_between(x21, x11, x12):
                if is_between(y11, y21, y22) or is_between(y12, y21, y22):
                    return [other_vector.start_point[0], self.start_point[1]]
        #  self is vertical, compare against horizontal
        elif self.direction[1] != 0 and other_vector.direction[1] == 0:
            if is_between(y21, y11, y12):
                if is_between(x11, x21, x22) or is_between(x12, x21, x22):
                    return [self.start_point[0], other_vector.start_point[1]]
        return None


def parse_line(line_string):
    direction_map = {
        'R': [1, 0],
        'D': [0, 1],
        'L': [-1, 0],
        'U': [0, -1],
    }
    direction = direction_map[line_string[0]]
    length = int(line_string[1:])
    return Vector(direction, length)


def populate_wire(wire):
    last_vector = Vector([0, 0], 0)
    for vector in wire:
        vector.start_point[0] = last_vector.start_point[0] + last_vector.direction[0] * last_vector.length
        vector.start_point[1] = last_vector.start_point[1] + last_vector.direction[1] * last_vector.length
        last_vector = vector


def parse_input(file):
    wire_strings = [line_string for line_string in file.readlines()]
    wire_one = [parse_line(line_string) for line_string in wire_strings[0].rstrip().split(',')]
    wire_two = [parse_line(line_string) for line_string in wire_strings[1].rstrip().split(',')]
    populate_wire(wire_one)
    populate_wire(wire_two)
    return wire_one, wire_two


def run_challenge_one(challenge_input):
    all_intersections = []
    for line_one in challenge_input[0]:
        for line_two in challenge_input[1]:
            intersection = line_one.check_for_intersection(line_two)
            if intersection is None:
                continue
            all_intersections.append(intersection)
    min_distance = min([abs(intersection[0]) + abs(intersection[1]) for intersection in all_intersections])
    return str(min_distance)


def run_challenge_two(challenge_input):
    min_distance = 999999

    distance_wire_one = 0
    for line_one in challenge_input[0]:
        distance_wire_one += line_one.length
        distance_wire_two = 0
        for line_two in challenge_input[1]:
            distance_wire_two += line_two.length
            intersection = line_one.check_for_intersection(line_two)
            if intersection is None:
                continue
            else:
                dist = distance_wire_one + distance_wire_two
                #  horizontal
                if line_one.direction[0] != 0:
                    length = line_one.direction[0] * line_one.length
                    actual_length = intersection[0] - line_one.start_point[0]
                    dist = dist - length + actual_length

                    length2 = line_two.direction[1] * line_two.length
                    actual_length2 = intersection[1] - line_two.start_point[1]
                    dist = dist - length2 + actual_length2
                    if dist < min_distance:
                        min_distance = dist
                else:
                    length = abs(line_two.direction[0]) * line_two.length
                    actual_length = abs(intersection[0] - line_two.start_point[0])
                    dist = dist - length + actual_length

                    length2 = abs(line_one.direction[1]) * line_one.length
                    actual_length2 = abs(intersection[1] - line_one.start_point[1])
                    dist = dist - length2 + actual_length2
                    if dist < min_distance:
                        min_distance = dist
    return str(min_distance)

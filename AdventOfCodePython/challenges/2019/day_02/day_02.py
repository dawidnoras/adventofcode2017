import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return [int(number) for number in file.read().strip().split(',')]


def run_int_code(int_code):
    current_line = 0
    while True:
        opt_code = int_code[current_line]
        if opt_code == 99:
            break
        first_operand_index = int_code[current_line + 1]
        second_operand_index = int_code[current_line + 2]
        result_index = int_code[current_line + 3]
        if opt_code == 1:
            int_code[result_index] = int_code[first_operand_index] + int_code[second_operand_index]
        elif opt_code == 2:
            int_code[result_index] = int_code[first_operand_index] * int_code[second_operand_index]
        current_line = current_line + 4
        if current_line >= len(int_code):
            break
    return int_code[0]


def run_challenge_one(challenge_input):
    challenge_input[1] = 12
    challenge_input[2] = 2
    return str(run_int_code(challenge_input))


def run_challenge_two(challenge_input):
    for noun in range(100):
        for verb in range(100):
            challenge_input_copy = challenge_input[:]
            challenge_input_copy[1] = noun
            challenge_input_copy[2] = verb
            result = run_int_code(challenge_input_copy)
            if result == 19690720:
                return str(100 * noun + verb)
    return 'Result not found'



import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class Image:
    def __init__(self, layers, width, height):
        self.layers = layers
        self.width = width
        self.height = height

    def get_data(self):
        data = []
        for y in range(self.height):
            row = []
            for x in range(self.width):
                row.append(self.get_color_at(x, y))
            data.append(row)
        return data

    def get_color_at(self, x, y):
        for layer in self.layers:
            color = layer.rows[y][x]
            if int(color) != 2:
                return int(color)
        return 2


class Layer:
    def __init__(self):
        self.rows = []

    def get_count_of(self, number):
        counter = 0
        for row in self.rows:
            for i in row:
                if int(i) == number:
                    counter += 1
        return counter


def parse_input(file):
    width, height = 25, 6
    data = file.read().rstrip('\n')
    layers = []
    while len(data) > 0:
        layer = Layer()
        for y in range(height):
            layer.rows.append(data[:width])
            data = data[width:]
        layers.append(layer)
    return Image(layers, width, height)


def run_challenge_one(challenge_input):
    layers_data = [{'layer': layer, 'zero_count': layer.get_count_of(0)} for layer in challenge_input.layers]
    min_zero_layer = layers_data = min(layers_data, key=lambda l: l['zero_count'])
    return str(min_zero_layer['layer'].get_count_of(1) * min_zero_layer['layer'].get_count_of(2))


def run_challenge_two(challenge_input):
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    lines = [str(row).lstrip('[').rstrip(']') + '\n' for row in challenge_input.get_data()]
    with open('{}/output_image.txt'.format(day), 'w') as file:
        file.writelines(lines)
    return 'Result in "output_image_txt" file'


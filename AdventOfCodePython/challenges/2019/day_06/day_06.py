import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


class SpaceObject:
    def __init__(self, name):
        self.name = name
        self.orbit_around = None
        self.children = []

    def get_child_count_recursive(self):
        return len(self.children) + sum([child.get_child_count_recursive() for child in self.children])

    def __repr__(self):
        return '{}-{}'.format(self.name, len(self.children))


def parse_input(file):
    space_objects = []
    objects = [objects.split(')') for objects in [line.rstrip('\n') for line in file.readlines()]]
    for obj in objects:
        center_object = obj[0]
        orbiting_object = obj[1]
        center_space_object = next(filter(lambda o: o.name == center_object, space_objects), None)
        if center_space_object is None:
            center_space_object = SpaceObject(center_object)
            space_objects.append(center_space_object)
        orbiting_space_object = next(filter(lambda o: o.name == orbiting_object, space_objects), None)
        if orbiting_space_object is None:
            orbiting_space_object = SpaceObject(orbiting_object)
            space_objects.append(orbiting_space_object)
        center_space_object.children.append(orbiting_space_object)
        orbiting_space_object.orbit_around = center_space_object
    return space_objects


def run_challenge_one(challenge_input):
    counter = 0
    for obj in challenge_input:
        counter += obj.get_child_count_recursive()
    return str(counter)


def run_challenge_two(challenge_input):
    you = next(filter(lambda o: o.name == 'YOU', challenge_input), None)
    san = next(filter(lambda o: o.name == 'SAN', challenge_input), None)

    def traverse(ln, lc):
        return {'node': ln, 'cost': lc}

    total_cost = 0
    san_found = False
    new_nodes = [traverse(you, -1)]
    traversed_nodes = []
    while not san_found:
        node = new_nodes[0]
        new_nodes.remove(node)

        if next(filter(lambda o: o['node'] == node['node'], traversed_nodes), None) is not None:
            print('error')
        n1 = node['node'].orbit_around
        if n1 is not None:
            if n1 == san:
                total_cost = node['cost']
                san_found = True
            if next(filter(lambda o: o['node'] == n1, traversed_nodes), None) is None:
                new_nodes.append(traverse(n1, node['cost'] + 1))
        for n in node['node'].children:
            if n == san:
                total_cost = node['cost']
                san_found = True
            if next(filter(lambda o: o['node'] == n, traversed_nodes), None) is None:
                new_nodes.append(traverse(n, node['cost'] + 1))
        traversed_nodes.append(node)
    return str(total_cost)

import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return 'parsed input'


def run_challenge_one(challenge_input):
    return 'not implemented challenge one'


def run_challenge_two(challenge_input):
    return 'not implemented challenge two'

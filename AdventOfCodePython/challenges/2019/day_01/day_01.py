import math
import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return [int(line) for line in file.readlines()]


def run_challenge_one(challenge_input):
    result = 0
    for number in challenge_input:
        result = result + math.floor(number / 3) - 2
    return str(result)


def run_challenge_two(challenge_input):
    result = 0
    for number in challenge_input:
        mass = number
        while mass > 0:
            required_fuel = math.floor(mass / 3) - 2
            if required_fuel <= 0:
                break
            result = result + required_fuel
            mass = required_fuel

    return str(result)

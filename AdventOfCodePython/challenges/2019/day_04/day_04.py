import os


def get_file_name():
    file, ext = os.path.splitext(__file__)
    return file


def get_date():
    file, ext = os.path.splitext(__file__)
    day, file = os.path.split(file)
    year, day = os.path.split(day)
    year = os.path.basename(year)
    return '{}/{}'.format(year, day)


def parse_input(file):
    return [int(number) for number in file.read().rstrip().split('-')]


def check_for_double_digit(number):
    s = str(number)
    for i in range(len(s) - 1):
        if s[i] == s[i + 1]:
            return True
    return False


def check_for_no_more_then_double_digit(number):
    s = str(number)
    for i in range(len(s) - 1):
        if s[i] == s[i + 1]:
            if len(s) > i + 2 and s[i] != s[i + 2] or len(s) == i + 2:
                if i > 0 and s[i] != s[i - 1] or i == 0:
                    return True
    return False


def check_for_non_decreasing(number):
    s = str(number)
    for i in range(len(s) - 1):
        if s[i] > s[i + 1]:
            return False
    return True


def run_challenge_one(challenge_input):
    counter = 0
    for n in range(challenge_input[1] - challenge_input[0] + 1):
        number = n + challenge_input[0]
        if check_for_double_digit(number) is False:
            continue
        if check_for_non_decreasing(number) is False:
            continue
        counter += 1
    return str(counter)


def run_challenge_two(challenge_input):
    counter = 0
    for n in range(challenge_input[1] - challenge_input[0] + 1):
        number = n + challenge_input[0]
        if check_for_no_more_then_double_digit(number) is False:
            continue
        if check_for_non_decreasing(number) is False:
            continue
        counter += 1
    return str(counter)

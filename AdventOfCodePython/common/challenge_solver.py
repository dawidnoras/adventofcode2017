def run(challenge_module, challenge_part, file_directory):
    in_path = file_directory + '/in.txt'
    with open(in_path, "r") as in_file:
        challenge_input = challenge_module.parse_input(in_file)

    if challenge_part == 1:
        print('----Running Challenge One----\n')
        output = challenge_module.run_challenge_one(challenge_input)
    else:
        print('----Running Challenge Two----\n')
        output = challenge_module.run_challenge_two(challenge_input)
    return output

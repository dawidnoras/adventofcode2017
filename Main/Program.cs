﻿using AdventOfCode2017.Common;
using AdventOfCode2017.Logic.Day1;
using AdventOfCode2017.Logic.Day2;
using AdventOfCode2017.Logic.Day3;
using AdventOfCode2017.Logic.Day4;
using AdventOfCode2017.Logic.Day5;
using AdventOfCode2017.Logic.Day6;

namespace AdventOfCode2017.Main
{
	class Program
	{
		static void Main(string[] args)
		{
			var challenge = new MemoryReallocation();
			Wrapper.Run(challenge, ChallengePart.Two);
		}
	}
}

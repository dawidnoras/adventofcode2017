﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day4
{
	public class HighEntropyPassphrases : ILogic<List<List<string>>>
	{
		public List<List<string>> ParseInput(string path)
		{
			var result = new List<List<string>>();
			var lines = File.ReadAllLines(path);
			foreach (var line in lines)
			{
				var word = line.Split(' ')
					.ToList();
				result.Add(word);
			}
			return result;
		}

		public string RunPartOne(List<List<string>> input)
		{
			int invalidCounter = 0;

			foreach (var passPhrase in input)
			{
				var wordSet = new HashSet<string>();
				foreach (var passWord in passPhrase)
				{
					if (wordSet.Contains(passWord))
					{
						invalidCounter++;
						break;
					}
					wordSet.Add(passWord);
				}
			}

			return (input.Count - invalidCounter).ToString();
		}

		private bool AreAnagram(string word, string otherWord)
		{
			if (word.Length != otherWord.Length)
			{
				return false;
			}

			var chars = word.ToCharArray().ToList();
			for (int i = 0; i < otherWord.Length; i++)
			{
				var c = otherWord[i];
				if (chars.Contains(c))
				{
					chars.Remove(c);
				}
				else
				{
					return false;
				}
			}

			return true;
		}

		public string RunPartTwo(List<List<string>> input)
		{
			int invalidCounter = 0;

			foreach (var passPhrase in input)
			{
				var wordSet = new HashSet<string>();
				foreach (var passWord in passPhrase)
				{
					bool found = false;
					foreach (string word in wordSet)
					{
						if (AreAnagram(passWord, word))
						{
							invalidCounter++;
							found = true;
							break;
						}
					}
					if(found)
					{
						break;
					}
					wordSet.Add(passWord);
				}
			}

			return (input.Count - invalidCounter).ToString();
		}
	}
}

﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day6
{
	public class MemoryReallocation : ILogic<List<int>>
	{
		public List<int> ParseInput(string path)
		{
			return File.ReadAllText(path)
				.Split('\t')
				.Select(number => int.Parse(number))
				.ToList();
		}

		public string RunPartOne(List<int> input)
		{
			var alreadyOccuredInputs = new HashSet<List<int>>
			{
				input
			};

			int counter = 0;
			var newInput = input.ToList();

			while (true)
			{
				newInput = newInput.ToList();
				counter++;

				int maxNumberOfBlocks = newInput.Max();
				int index = newInput.IndexOf(maxNumberOfBlocks);
				newInput[index] = 0;
				for (int i = maxNumberOfBlocks; i > 0; i--)
				{
					index = (++index) % input.Count;
					newInput[index] += 1;
				}

				if (IsSequenceEqualToAny(alreadyOccuredInputs, newInput))
				{
					break;
				}
				alreadyOccuredInputs.Add(newInput);
			}

			return counter.ToString();
		}

		public string RunPartTwo(List<int> input)
		{
			var alreadyOccuredInputs = new HashSet<List<int>>
			{
				input
			};

			int counter = 0;
			var newInput = input.ToList();

			while (true)
			{
				newInput = newInput.ToList();
				counter++;

				int maxNumberOfBlocks = newInput.Max();
				int index = newInput.IndexOf(maxNumberOfBlocks);
				newInput[index] = 0;
				for (int i = maxNumberOfBlocks; i > 0; i--)
				{
					index = (++index) % input.Count;
					newInput[index] += 1;
				}

				if (IsSequenceEqualToAny(alreadyOccuredInputs, newInput))
				{
					break;
				}
				alreadyOccuredInputs.Add(newInput);
			}

			return RunPartOne(newInput);
		}

		private bool IsSequenceEqualToAny(HashSet<List<int>> inputs, List<int> newInput)
		{
			foreach (var input in inputs)
			{
				if (input.SequenceEqual(newInput))
				{
					return true;
				}
			}
			return false;
		}
	}
}

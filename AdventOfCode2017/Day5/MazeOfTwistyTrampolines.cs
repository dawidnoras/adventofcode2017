﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day5
{
	public class MazeOfTwistyTrampolines : ILogic<List<int>>
	{
		public List<int> ParseInput(string path)
		{
			var lines = File.ReadAllLines(path);
			return lines.Select(line => int.Parse(line)).ToList();
		}

		public string RunPartOne(List<int> input)
		{
			int currentIndex = 0;
			int jumpCounter = 0;
			do
			{
				int newValue = input[currentIndex] + 1;
				int newIndex = currentIndex + input[currentIndex];
				input[currentIndex] = newValue;
				currentIndex = newIndex;
				jumpCounter++;
			}
			while (currentIndex >= 0 && currentIndex < input.Count);

			return jumpCounter.ToString();
		}

		public string RunPartTwo(List<int> input)
		{
			int currentIndex = 0;
			int jumpCounter = 0;
			do
			{
				int newIndex = currentIndex + input[currentIndex];
				int newValue = input[currentIndex] + (input[currentIndex] >= 3 ? -1 : 1);
				input[currentIndex] = newValue;
				currentIndex = newIndex;
				jumpCounter++;
			}
			while (currentIndex >= 0 && currentIndex < input.Count);

			return jumpCounter.ToString();
		}
	}
}

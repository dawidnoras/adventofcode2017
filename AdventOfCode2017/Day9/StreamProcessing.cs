﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day9
{
	public class StreamProcessing : ILogic<StreamReader>
	{
		public StreamReader ParseInput(string path)
		{
			return File.OpenText(path);
		}

		public string RunPartOne(StreamReader input)
		{
			int groupCounter = 0;

			int scoreMultiplier = 1;
			int totalScore = 0;
			using (input)
			{
				while (!input.EndOfStream)
				{
					var character = (char)input.Read();
					//Console.WriteLine(character);

					switch (character)
					{
						case '!':
							{
								//Skip character
								input.Read();
								//Console.WriteLine("I: " + character);
							}
							break;
						case '<':
							{
								//Skip garbage
								while (true)
								{
									character = (char)input.Read();
									//Console.WriteLine("G: " + character);
									if (character == '!')
									{
										//Skip character
										input.Read();
										//Console.WriteLine("G: " + character);
									}
									if (character == '>')
									{
										break;
									}
								}
							}
							break;
						case ',':
							{
								//Do nothing
							}
							break;
						case '{':
							{
								groupCounter++;
								totalScore += scoreMultiplier;
								scoreMultiplier++;
							}
							break;
						case '}':
							{
								scoreMultiplier--;
							}
							break;
						default:
							{

							}
							break;
					}
					
				}
			}
			return totalScore.ToString();
		}

		public string RunPartTwo(StreamReader input)
		{
			int garbageCounter = 0;

			int groupCounter = 0;

			int scoreMultiplier = 1;
			int totalScore = 0;
			using (input)
			{
				while (!input.EndOfStream)
				{
					var character = (char)input.Read();
					//Console.WriteLine(character);

					switch (character)
					{
						case '!':
							{
								//Skip character
								input.Read();
								//Console.WriteLine("I: " + character);
							}
							break;
						case '<':
							{
								//Skip garbage
								while (true)
								{
									character = (char)input.Read();
									//Console.WriteLine("G: " + character);

									if (character == '!')
									{
										//Skip character
										input.Read();
										//Console.WriteLine("G: " + character);
									}
									else if (character == '>')
									{
										break;
									}
									else
									{
										garbageCounter++;
									}
								}
							}
							break;
						case ',':
							{
								//Do nothing
							}
							break;
						case '{':
							{
								groupCounter++;
								totalScore += scoreMultiplier;
								scoreMultiplier++;
							}
							break;
						case '}':
							{
								scoreMultiplier--;
							}
							break;
						default:
							{

							}
							break;
					}

				}
			}
			return garbageCounter.ToString();
		}
	}
}

﻿using AdventOfCode2017.Common;
using System.IO;

namespace AdventOfCode2017.Logic.Day1
{
	public class InverseCaptcha : ILogic<string>
	{
		public string ParseInput(string path)
		{
			return File.ReadAllText(path);
		}

		public string RunPartOne(string input)
		{
			char lastCharacter = input[0];
			int sum = 0;
			for (int i = 1; i < input.Length; i++)
			{
				if (input[i] == lastCharacter)
				{
					sum += int.Parse(input[i].ToString());
				}
				lastCharacter = input[i];
			}

			if (input[0] == input[input.Length - 1])
			{
				sum += int.Parse(input[0].ToString());
			}

			return sum.ToString();
		}

		public string RunPartTwo(string input)
		{
			int halfLength = input.Length / 2;

			int sum = 0;
			for (int i = 0; i < input.Length; i++)
			{
				char halfAraoundCharacter = input[(i + halfLength) % input.Length];
				if (input[i] == halfAraoundCharacter)
				{
					sum += int.Parse(input[i].ToString());
				}
				
			}

			return sum.ToString();
		}
	}
}

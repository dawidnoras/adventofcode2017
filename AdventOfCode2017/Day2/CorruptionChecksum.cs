﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2017.Logic.Day2
{
	public class CorruptionChecksum : ILogic<List<List<int>>>
	{
		public List<List<int>> ParseInput(string path)
		{
			var result = new List<List<int>>();
			var lines = File.ReadAllLines(path);
			foreach (var line in lines)
			{
				var numbers = line.Split('\t')
					.Select(str => int.Parse(str))
					.ToList();
				result.Add(numbers);
			}
			return result;
		}

		public string RunPartOne(List<List<int>> input)
		{
			int sum = 0;
			foreach (var row in input)
			{
				sum += row.Max() - row.Min();
			}
			return sum.ToString();
		}

		public string RunPartTwo(List<List<int>> input)
		{
			int sum = 0;
			foreach (var row in input)
			{
				bool foundPair = false;
				for (int i = 0; i < row.Count; i++)
				{
					for (int j = i + 1; j < row.Count; j++)
					{
						int lowerValue = row[i] <= row[j] ? row[i] : row[j];
						int higherValue = row[i] <= row[j] ? row[j] : row[i];
						if (higherValue % lowerValue == 0)
						{
							sum += higherValue / lowerValue;
							foundPair = true;
							break;
						}
					}

					if (foundPair)
					{
						break;
					}
				}
			}
			return sum.ToString();
		}
	}
}

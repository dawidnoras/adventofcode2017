﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day8
{
	public class InputData
	{
		public List<Instruction> instructions;
		public List<Register> registers;
	}
}

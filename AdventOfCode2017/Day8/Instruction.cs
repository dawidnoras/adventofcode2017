﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2017.Logic.Day8
{
	public class Instruction
	{
		public string registerName;

		public Register register;

		private string stringAction;

		private Action registerAction;

		private int Value;

		public string registerNameIf;

		private Register registerIf;

		private string ifAction;

		private int checkValue;

		private Func<bool> canExecuteFunc;

		public Instruction(string line)
		{
			var parts = line.Split(' ');
			registerName = parts[0];
			stringAction = parts[1];
			Value = int.Parse(parts[2]);

			registerNameIf = parts[4];
			checkValue = int.Parse(parts[6]);

			ifAction = parts[5];
		}

		public void Execute()
		{
			if (canExecuteFunc())
			{
				registerAction();
			}
		}

		public void PopulateWithRegisters(Register register, Register registerIf)
		{
			this.register = register;

			switch (stringAction)
			{
				case "inc":
					registerAction = () => register.Add(Value);
					break;
				case "dec":
					registerAction = () => register.Substract(Value);
					break;
			}

			this.registerIf = registerIf;

			switch (ifAction)
			{
				case ">":
					canExecuteFunc = () => registerIf.Value > checkValue;
					break;
				case "<":
					canExecuteFunc = () => registerIf.Value < checkValue;
					break;
				case ">=":
					canExecuteFunc = () => registerIf.Value >= checkValue;
					break;
				case "<=":
					canExecuteFunc = () => registerIf.Value <= checkValue;
					break;
				case "==":
					canExecuteFunc = () => registerIf.Value == checkValue;
					break;
				case "!=":
					canExecuteFunc = () => registerIf.Value != checkValue;
					break;
			}
		}
	}
}
﻿namespace AdventOfCode2017.Logic.Day8
{
	public class Register
	{
		public string Name { get; private set; }
		public int Value { get; private set; }

		public Register(string name)
		{
			Name = name;
		}

		public void Add(int value)
		{
			Value += value;
		}

		public void Substract(int value)
		{
			Value -= value;
		}
	}
}

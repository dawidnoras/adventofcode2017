﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day8
{
	public class IHeardYouLikeRegisters : ILogic<InputData>
	{
		public InputData ParseInput(string path)
		{
			var lines = File.ReadAllLines(path);
			var instructions = new List<Instruction>();
			foreach (var line in lines)
			{
				instructions.Add(new Instruction(line));
			}

			var registers = new List<Register>();
			foreach (var instruction in instructions)
			{
				var register = registers.Find(r => r.Name == instruction.registerName);
				if (register == null)
				{
					register = new Register(instruction.registerName);
					registers.Add(register);
				}

				var registerIf = registers.Find(r => r.Name == instruction.registerNameIf);
				if (registerIf == null)
				{
					registerIf = new Register(instruction.registerNameIf);
					registers.Add(registerIf);
				}
				instruction.PopulateWithRegisters(register, registerIf);
			}
			return new InputData() { instructions = instructions, registers = registers};
		}

		public string RunPartOne(InputData input)
		{
			foreach (var instruction in input.instructions)
			{
				instruction.Execute();
			}
			var maxRegister = input.registers.Max(register => register.Value);
			return maxRegister.ToString();
		}

		public string RunPartTwo(InputData input)
		{
			var maxRegisterValue = 0;
			foreach (var instruction in input.instructions)
			{
				instruction.Execute();
				if (instruction.register.Value > maxRegisterValue)
				{
					maxRegisterValue = instruction.register.Value;
				}
			}
			return maxRegisterValue.ToString();
		}
	}
}

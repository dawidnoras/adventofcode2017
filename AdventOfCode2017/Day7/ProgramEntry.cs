﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017.Logic.Day7
{
	public class ProgramEntry
	{
		public string Name { get; private set; }

		public ProgramEntry Parent { get; set; }

		public List<ProgramEntry> Children { get; private set; } = new List<ProgramEntry>();

		public int Weight { get; private set; }

		public int TowerWeight { get; set; } = -1;

		private List<string> childrenStrings = new List<string>();

		public ProgramEntry(string data)
		{
			var splitData = data.Split(' ');
			Name = splitData[0];
			var weightString = splitData[1].Substring(1, splitData[1].Length - 2);
			Weight = int.Parse(weightString);
			if (splitData.Length > 2)
			{
				for (int i = 3; i < splitData.Length; i++)
				{
					childrenStrings.Add(splitData[i].Replace(",", ""));
				}
			}
		}

		public void PopulateChildren(List<ProgramEntry> programs)
		{
			foreach (var childString in childrenStrings)
			{
				var child = programs.Find(x => x.Name == childString);
				Children.Add(child);
				child.Parent = this;
			}
		}

		public int CalculateTowerWeight()
		{
			if (Children.Count == 0)
			{
				TowerWeight = Weight;
				return TowerWeight;
			}

			if (TowerWeight != -1)
			{
				return TowerWeight;
			}

			TowerWeight =  Weight + Children.Sum(child => child.CalculateTowerWeight());
			return TowerWeight;
		}
	}
}
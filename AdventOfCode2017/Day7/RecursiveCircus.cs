﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day7
{
	public class RecursiveCircus : ILogic<IEnumerable<ProgramEntry>>
	{
		public IEnumerable<ProgramEntry> ParseInput(string path)
		{
			var lines = File.ReadAllLines(path);
			foreach (var line in lines)
			{
				yield return new ProgramEntry(line);
			}
		}

		public string RunPartOne(IEnumerable<ProgramEntry> input)
		{
			var programs = input.ToList();
			foreach (var p in programs)
			{
				p.PopulateChildren(programs);
			}

			var rootProgram = programs[0];
			while (rootProgram.Parent != null)
			{
				rootProgram = rootProgram.Parent;
			}

			return rootProgram.Name;
		}

		public string RunPartTwo(IEnumerable<ProgramEntry> input)
		{
			var programs = input.ToList();
			foreach (var p in programs)
			{
				p.PopulateChildren(programs);
			}

			var rootProgram = programs[0];
			while (rootProgram.Parent != null)
			{
				rootProgram = rootProgram.Parent;
			}

			programs.ForEach(program => program.CalculateTowerWeight());

			ProgramEntry wrongWeightParent = null;
			foreach (var program in programs)
			{
				if (program.Children.Count > 0)
				{
					var towerWeight = program.Children[0].TowerWeight;
					for (int i = 1; i < program.Children.Count; i++)
					{
						if (program.Children[i].TowerWeight != towerWeight)
						{
							wrongWeightParent = program;
							break;
						}
					}
					if (wrongWeightParent != null)
					{
						break;
					}
				}
			}

			var pp = programs.Find(x => x.Name == "gexwzw");

			ProgramEntry wrongWeightProgram = null;
			var diff = 0;
			if (wrongWeightParent.Children.FindAll(child => child.TowerWeight == wrongWeightParent.Children[0].TowerWeight).Count > 1)
			{
				wrongWeightProgram = wrongWeightParent.Children.Find(child => child.TowerWeight != wrongWeightParent.Children[0].TowerWeight);
				diff = wrongWeightParent.Children[1].TowerWeight - wrongWeightParent.Children[0].TowerWeight;
			}
			else
			{
				wrongWeightProgram = wrongWeightParent.Children[0];
				diff = wrongWeightParent.Children[0].TowerWeight - wrongWeightParent.Children[1].TowerWeight;
			}

			int neededProgramWeight = wrongWeightProgram.Weight - diff;

			var s = $"Program named {wrongWeightParent.Name} with weight {wrongWeightParent.TowerWeight} is a sum of ";
			foreach (var program in wrongWeightParent.Children)
			{
				s += $"{program.Name}({program.TowerWeight}), ";
			}
			s += $"{Environment.NewLine}{neededProgramWeight}";

			return s;
		}
	}
}

﻿using AdventOfCode2017.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2017.Logic.Day3
{
	public class SpiralMemory : ILogic<int>
	{
		public int ParseInput(string path)
		{
			return int.Parse(File.ReadAllText(path));
		}

		public string RunPartOne(int input)
		{
			int dimension = 1000;

			var table = new int[dimension, dimension];
			int middleIndex = dimension / 2;

			currentDirectionIndex = -1;
			int stepCount = 1;

			int x = middleIndex;
			int y = middleIndex;

			int number = 1;
			table[x, y] = number;
			number++;

			bool found = false;
			while (!found)
			{
				for (int moveCount = 0; moveCount < 2; moveCount++)
				{
					var direction = GetNextDirection();
					for (int step = 0; step < stepCount; step++)
					{
						x += direction.x;
						y += direction.y;

						table[x, y] = number;

						if (number == input)
						{
							found = true;
							break;
						}

						number++;
					}

					if (found)
					{
						break;
					}
				}

				stepCount++;
			}

			int distance = Math.Abs(middleIndex - x) + Math.Abs(middleIndex - y);

			return $"Input: {input}{Environment.NewLine}Distance: {distance}";
		}

		public string RunPartTwo(int input)
		{
			int dimension = 1000;

			var table = new int[dimension, dimension];
			int middleIndex = dimension / 2;

			currentDirectionIndex = -1;
			int stepCount = 1;

			int x = middleIndex;
			int y = middleIndex;

			table[x, y] = 1;
			int number = 0;
			bool found = false;
			while (!found)
			{
				for (int moveCount = 0; moveCount < 2; moveCount++)
				{
					var direction = GetNextDirection();
					for (int step = 0; step < stepCount; step++)
					{
						x += direction.x;
						y += direction.y;

						number = table[x, y] = GetSumOfNeighbours(table, x, y);

						if (number > input)
						{
							found = true;
							break;
						}
					}

					if (found)
					{
						break;
					}
				}

				stepCount++;
			}

			return $"Input: {input}{Environment.NewLine}Result: {number}";
		}

		private List<(int x, int y)> directions = new List<(int x, int y)>
		{
			(1, 0),
			(0, 1),
			(-1, 0),
			(0, -1),
		};

		private int currentDirectionIndex;
		private (int x, int y) GetNextDirection()
		{
			currentDirectionIndex = (++currentDirectionIndex) % 4;
			return directions[currentDirectionIndex];
		}

		private int GetSumOfNeighbours(int[,] table, int x, int y)
		{
			int sum = 0;
			for (int i = -1; i < 2; i++)
			{
				for (int j = -1; j < 2; j++)
				{
					sum += table[x + i, y + j];
				}
			}
			return sum;
		}
	}
}

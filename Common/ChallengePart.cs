﻿namespace AdventOfCode2017.Common
{
	public enum ChallengePart
	{
		One,
		Two
	}
}

﻿using System;
using System.IO;

namespace AdventOfCode2017.Common
{
	public static class Wrapper
	{
		public static void Run<T>(ILogic<T> logic, ChallengePart challengePart = ChallengePart.One, string inPath = "in.txt", string outPath = "out.txt")
		{
			var input = logic.ParseInput(inPath);

			string result = string.Empty;
			switch (challengePart)
			{
				case ChallengePart.One:
					result = logic.RunPartOne(input);
					break;
				case ChallengePart.Two:
					result = logic.RunPartTwo(input);
					break;
			}

			File.WriteAllText(outPath, result);
			Console.WriteLine($"Result: {Environment.NewLine}{result}");
			Console.ReadLine();
		}
	}
}

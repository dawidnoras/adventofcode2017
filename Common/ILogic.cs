﻿namespace AdventOfCode2017.Common
{
	public interface ILogic<T>
	{
		T ParseInput(string path);

		string RunPartOne(T input);

		string RunPartTwo(T input);
	}
}
